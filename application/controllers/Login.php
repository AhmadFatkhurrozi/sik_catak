<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (!empty($this->session->userdata('get_id'))) {
            redirect(base_url('dashboard'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $this->is__login();
        $this->load->view('layout/login');
    }

    function login()
    {
        $username   = $this->input->post('username');
        $pass       = $this->input->post('password');

        $cek = $this->M_data->auth($username, $pass);

        if( $cek->num_rows() != 0 ){

            $id = $cek->row()->id;
            
            $query = $this->db->where('id', $id)->get('users');

            if( $query->num_rows() != 0 )
            {
                $array = array(
                    'get_id'        => $id,
                    'get_nama'  => $query->row()->nama,
                    'get_username'=> $query->row()->username,
                    'get_level' => $query->row()->level,
                );
                
                $this->session->set_userdata( $array );

                if ($query->row()->level == 'admin') {
                    redirect(base_url("dashboard"),'refresh');
                }else{
                    redirect(base_url(),'refresh');
                }

            }
        }
        else
        {
            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Upps! username atau password Salah.</span>
                </p');  
            
            redirect( $this->kembali() );
        }
    }

    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }
}
