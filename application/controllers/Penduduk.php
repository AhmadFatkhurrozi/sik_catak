<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penduduk extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']  = "Data Penduduk";
        
        $data['konten']  = $this->load->view('penduduk/main', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }
}
