<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        if (!empty($this->session->userdata('get_id'))) {
            redirect(base_url("pembuatan-surat"),'refresh');
        }else{

            $data['judul']   = "Pengajuan Surat";
            $data['konten']  = $this->load->view('site/surat/main', $data, TRUE);

            $this->load->view('site/master', $data, FALSE);
        }
    }

    public function cekNik()
    {   
        $nik       = $this->input->post('nik');
        $cek = $this->M_data->auth_penduduk($nik);

        if( $cek->num_rows() != 0 ){

            $id = $cek->row()->id;
            
            $query = $this->db->where('id', $id)->get('penduduk');

            if( $query->num_rows() != 0 )
            {
                    $array = array(
                        'penduduk_id'        => $id,
                        'penduduk_nama'  => $query->row()->nama,
                        'penduduk_nik'=> $query->row()->nik,
                    );
                    
                    $this->session->set_userdata( $array );

                    redirect(base_url("pembuatan-surat"),'refresh');

                }
            }
            else
            {
                $this->session->set_flashdata('msg', 
                    '<p class="text-danger" style="margin-top: 10px;">
                        <span><i class="fa fa-times"></i>Upps! NIK tidak ditemukan.</span>
                    </p');  
                
                redirect( $this->kembali() );
            }
    }

    public function pilihSurat()
    {
        $data['judul']   = "Pengajuan Surat";
        $data['konten']  = $this->load->view('site/surat/pilihSurat', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }
}
