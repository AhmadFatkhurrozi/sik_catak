<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penduduk extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']   = "Data Penduduk";
        $data['dusun'] = $this->db->get('dusun');
        $data['konten']  = $this->load->view('site/penduduk/main', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function cariPenduduk()
    {   
        if ($this->input->post('dusun_id') !== "") {
            $this->db->where('dusun_id', $this->input->post('dusun_id'));
        }

        if ($this->input->post('rw_id') !== "") {
            $this->db->where('rw_id', $this->input->post('rw_id'));
        }

        if ($this->input->post('rt_id') !== "") {
            $this->db->where('rt_id', $this->input->post('rt_id'));
        }

        $query = $this->db->where('status', 'tinggal ditempat')->get('penduduk')->result();

        if (count($query) > 0) {
            $callback = ['status'=>'success', 'data'=>$query, 'usia'=>$this->input->post('usia')];
        }else{
            $callback = ['status'=>'error', 'data'=>"", 'usia'=>''];
        }

        echo json_encode($callback);
    }
}
