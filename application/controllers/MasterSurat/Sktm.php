<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sktm extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']   = "SKTM";
        $data['konten']  = $this->load->view('site/surat/sktm', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function simpan()
    {
        $cekSurat = $this->db->get('sktm')->num_rows();
        if ($cekSurat > 0) {
            $cek = $this->db->select('no_surat')->order_by('id_sktm','DESC')->limit(1)->get('sktm')->result_array();
            $ex = explode('/', $cek[0]['no_surat']);
            $no_urut = $ex[1]+1;

            $noSurat = '100/'.$no_urut.'/415.71.14/'.date('Y');
        }else{
            $noSurat = '100/1/415.71.14/'.date('Y');
        }

        $data = array(
            'nik'  => $this->input->post('nik'),
            'tgl_dibuat' => date('Y-m-d'),
            'keterangan' => $this->input->post('keterangan'),
            'no_surat'  => $noSurat,
        );

        $this->M_data->create('sktm', $data);
        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Membuat Surat!</span>
                </div>');

        redirect(base_url('sktm/'.$this->input->post('nik')));
    }

    public function detailSurat()
    {
        $data['judul']   = "Detail SKTM";
        $data['data']    = $this->db->select('p.nik, p.nama, p.dusun, p.rt, p.rw, p.agama, p.pekerjaan, sktm.keterangan')
                                ->where('sktm.nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=sktm.nik')
                                ->get('sktm')->row();
        $data['konten']  = $this->load->view('site/surat/detailSktm', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function cetak()
    {
        $data['judul']   = "SKTM";
        $data['data']    = $this->db->select('p.*, sktm.keterangan, sktm.tgl_dibuat, sktm.no_surat')
                                ->where('sktm.nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=sktm.nik')
                                ->get('sktm')->row();

        $this->load->view('site/surat/cetakSktm', $data, FALSE);
        
        $html = $this->output->get_output();
        
        $this->load->library('dompdf_gen');
        
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        ob_end_clean();
        $this->dompdf->stream("sktm".$this->uri->segment(2).".pdf", array('Attachment' => 0));
    }
}
