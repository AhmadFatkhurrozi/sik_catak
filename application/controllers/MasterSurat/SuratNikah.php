<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratNikah extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']   = "Pengantar Nikah";
        $data['konten']  = $this->load->view('site/surat/nikah', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function simpan()
    {   
        $cekSurat = $this->db->get('pengantar_nikah')->num_rows();
        if ($cekSurat > 0) {
            $cek = $this->db->select('no_surat')->order_by('id_pengantar_nikah','DESC')->limit(1)->get('pengantar_nikah')->result_array();
            $ex = explode('/', $cek[0]['no_surat']);
            $no_urut = $ex[1]+1;

            $noSurat = '400/'.$no_urut.'/415.17.14/'.date('Y');
        }else{
            $noSurat = '400/1/415.17.14/'.date('Y');
        }

        $data = array(
            'nik'  => $this->input->post('nik'),
            'nik_ayah'  => $this->input->post('nik_ayah'),
            'nik_ibu'  => $this->input->post('nik_ibu'),
            'tgl_dibuat'   => date('Y-m-d'),
            'status_kawin'  => $this->input->post('status_kawin'),
            'no_surat'  => $noSurat,
        );

        $this->M_data->create('pengantar_nikah', $data);


       $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Membuat Surat!</span>
                </div>');

        redirect(base_url('surat-nikah/'.$this->input->post('nik')));
    }

    public function detailSurat()
    {
        $data['judul']   = "Detail Surat Pengantar Nikah";
        $data['data']    = $this->db->select('p.nama, p.dusun, p.rw, p.rt, pengantar_nikah.*')->where('pengantar_nikah.nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=pengantar_nikah.nik')
                                ->get('pengantar_nikah')->row();
        $data['konten']  = $this->load->view('site/surat/detailNikah', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function cetak()
    {
        $data['judul']   = "Surat Pengantar Nikah";
        $data['data']    = $this->db->select('p.nama, p.dusun, p.rw, p.rt, p.jk, p.tempat_lahir, p.tgl_lahir, p.kwarganegaraan, p.agama, p.pekerjaan, pengantar_nikah.*')->where('pengantar_nikah.nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=pengantar_nikah.nik')
                                ->get('pengantar_nikah')->row();

        $this->load->view('site/surat/cetakNikah', $data, FALSE);
        
        $html = $this->output->get_output();
        
        $this->load->library('dompdf_gen');
        
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        ob_end_clean();
        $this->dompdf->stream("surat-pengantar-nikah-".$this->uri->segment(2).".pdf", array('Attachment' => 0));
    }
}
