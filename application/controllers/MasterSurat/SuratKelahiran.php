<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratKelahiran extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']   = "Surat Kelahiran";
        $data['konten']  = $this->load->view('site/surat/kelahiran', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function simpan()
    {
        $cekSurat = $this->db->get('surat_kelahiran')->num_rows();
        if ($cekSurat > 0) {
            $cek = $this->db->select('no_surat')->order_by('id_kelahiran','DESC')->limit(1)->get('surat_kelahiran')->result_array();
            $ex = explode('/', $cek[0]['no_surat']);
            $no_urut = $ex[1]+1;

            $noSurat = '100/'.$no_urut.'/415.71.14/'.date('Y');
        }else{
            $noSurat = '100/1/415.71.14/'.date('Y');
        }

        $config['upload_path']      = './upload/';
        $config['allowed_types']    = 'jpg|png|jpeg|gif';
        $config['file_name']        = md5(date('YmdHis'));
        $config['max_size']         = '4056';
        $config['overwrite']        = TRUE;
        
        $this->load->library('upload', $config);

        $data = array(
            'tgl_dibuat' => date('Y-m-d'),
            'nama_anak' => $this->input->post('nama_anak'),
            'tmpt_lahir' => $this->input->post('tmpt_lahir'),
            'tgl_lahir' => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
            'agama' => $this->input->post('agama'),
            'jk' => $this->input->post('jk'),
            'no_urut' => $this->input->post('no_urut'),
            'nama_ibu' => $this->input->post('nama_ibu'),
            'umur_ibu' => $this->input->post('umur_ibu'),
            'nama_ayah' => $this->input->post('nama_ayah'),
            'umur_ayah' => $this->input->post('umur_ayah'),
            'alamat' => $this->input->post('alamat'),
            'no_surat'  => $noSurat,
        );

        if ($this->upload->do_upload('foto_kms')) {
            $this->upload->do_upload('foto_kms');
            $data['foto_kms'] = $this->upload->data("file_name");
        }

        $this->M_data->create('surat_kelahiran', $data);
        $id = $this->db->insert_id();
        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Membuat Surat!</span>
                </div>');

        redirect(base_url('surat-kelahiran/'.$id));
    }

    public function detailSurat()
    {
        $data['judul']   = "Detail Surat Kelahiran";
        $data['data']    = $this->db->where('surat_kelahiran.id_kelahiran', $this->uri->segment(2))
                                ->get('surat_kelahiran')->row();
        $data['konten']  = $this->load->view('site/surat/detailKelahiran', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function cetak()
    {
        $data['judul']   = "Surat Kelahiran";
        $data['data']    = $this->db->where('surat_kelahiran.id_kelahiran', $this->uri->segment(2))
                                ->get('surat_kelahiran')->row();

        $this->load->view('site/surat/cetakKelahiran', $data, FALSE);
        
        $html = $this->output->get_output();
        
        $this->load->library('dompdf_gen');
        
        $this->dompdf->load_html($html);
        $this->dompdf->set_paper("F4", 'portrait');
        $this->dompdf->render();
        ob_end_clean();
        $this->dompdf->stream("surat-kelahiran-".$this->uri->segment(2).".pdf", array('Attachment' => 0,));
    }
}
