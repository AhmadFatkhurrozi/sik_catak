<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skck extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']   = "SKCK";
        $data['konten']  = $this->load->view('site/surat/skck', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function simpan()
    {   
        $cekSurat = $this->db->get('skck')->num_rows();
        if ($cekSurat > 0) {
            $cek = $this->db->select('no_surat')->order_by('id_skck','DESC')->limit(1)->get('skck')->result_array();
            $ex = explode('/', $cek[0]['no_surat']);
            $no_urut = $ex[1]+1;

            $noSurat = '100/'.$no_urut.'/415.71.14/'.date('Y');
        }else{
            $noSurat = '100/1/415.71.14/'.date('Y');
        }

        $data = array(
            'nik'  => $this->input->post('nik'),
            'tgl_dibuat' => date('Y-m-d'),
            'keterangan' => $this->input->post('keterangan'),
            'no_surat'  => $noSurat,
        );

        $this->M_data->create('skck', $data);
        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Membuat Surat!</span>
                </div>');

        redirect(base_url('skck/'.$this->input->post('nik')));
    }

    public function detailSurat()
    {
        $data['judul']   = "Detail SKCK";
        $data['data']    = $this->db->select('p.nik, p.nama, p.dusun, p.rt, p.rw, p.agama, p.pekerjaan, skck.keterangan')
                                ->where('skck.nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=skck.nik')
                                ->get('skck')->row();
        $data['konten']  = $this->load->view('site/surat/detailSkck', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function cetak()
    {
        $data['judul']   = "SKCK";
        $data['data']    = $this->db->select('p.*, skck.keterangan, skck.tgl_dibuat, skck.no_surat')
                                ->where('skck.nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=skck.nik')
                                ->get('skck')->row();

        $this->load->view('site/surat/cetakSkck', $data, FALSE);
        
        $html = $this->output->get_output();
        
        $this->load->library('dompdf_gen');
        
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        ob_end_clean();
        $this->dompdf->stream("skck-".$this->uri->segment(2).".pdf", array('Attachment' => 0));
    }
}
