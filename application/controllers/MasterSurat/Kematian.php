<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kematian extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']   = "Surat Keterangan Kematian";
        $data['konten']  = $this->load->view('site/surat/kematian', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function simpan()
    {   
        $cekSurat = $this->db->get('surat_kematian')->num_rows();
        if ($cekSurat > 0) {
            $cek = $this->db->select('no_surat')->order_by('id_kematian','DESC')->limit(1)->get('surat_kematian')->result_array();
            $ex = explode('/', $cek[0]['no_surat']);
            $no_urut = $ex[1]+1;

            $noSurat = '100/'.$no_urut.'/415.71.14/'.date('Y');
        }else{
            $noSurat = '100/1/415.71.14/'.date('Y');
        }

        $data = array(
            'nik_jenazah'  => $this->input->post('nik'),
            'tgl_dibuat'   => date('Y-m-d'),
            'hari_meninggal'=> $this->input->post('hari'),
            'tgl_meninggal'     => date('Y-m-d', strtotime($this->input->post('tanggal'))),
            'tmpt_meninggal'  => $this->input->post('tempat'),
            'sebab_meninggal' => $this->input->post('sebab'),
            'nama_pelapor'  => $this->input->post('nama_pelapor'),
            'no_surat'  => $noSurat,
        );

        $this->M_data->create('surat_kematian', $data);
        

        $data2 = array('status'=> 'meninggal',);
        $id = array('nik' => $this->input->post('nik'));
        $this->M_data->ubah_data($id, $data2,'penduduk');


        if (!empty($this->session->userdata('get_id'))) {
            $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> berhasil menyimpan data!</span>
                </div>');

            redirect(base_url('penduduk-meninggal'));
        }else{
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-success" style="margin-top: 10px;">
                        <span><i class="fa fa-check"></i> Berhasil Membuat Surat!</span>
                    </div>');

            redirect(base_url('surat-kematian/'.$this->input->post('nik')));
        }
    }

    public function detailSurat()
    {
        $data['judul']   = "Detail Surat Keterangan Kematian";
        $data['data']    = $this->db->where('nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=surat_kematian.nik_jenazah')
                                ->get('surat_kematian')->row();
        $data['konten']  = $this->load->view('site/surat/detailKematian', $data, TRUE);

        $this->load->view('site/master', $data, FALSE);
    }

    public function cetak()
    {
        $data['judul']   = "Surat Keterangan Kematian";
        $data['data']    = $this->db->where('nik', $this->uri->segment(2))
                                ->join('penduduk as p', 'p.nik=surat_kematian.nik_jenazah')
                                ->get('surat_kematian')->row();

        $this->load->view('site/surat/cetakKematian', $data, FALSE);
        
        $html = $this->output->get_output();
        
        $this->load->library('dompdf_gen');
        
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        ob_end_clean();
        $this->dompdf->stream("surat-keterangan-kematian-".$this->uri->segment(2).".pdf", array('Attachment' => 0));
    }
}
