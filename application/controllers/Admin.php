<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

 		$this->load->library('session');
		$this->load->helper('me');
		$this->load->model('M_data');
		$this->load->helper('tgl_indo');
		$this->load->helper(array('form', 'url'));
	}

	public function is__login()
	{
		if (empty($this->session->userdata('get_id'))) {

			$this->session->set_flashdata('msg', 
				'<p class="text-danger" style="margin-top: 10px;">
					<span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
				</p');

			redirect(base_url('login'));
		}else{
			redirect($this->kembali());
		}
	}

	public function kembali()
	{

		return $this->input->server('HTTP_REFERER');
	}

	public function index()
	{
		$this->is__login();
		$this->load->view('dashboard');
	}
}
