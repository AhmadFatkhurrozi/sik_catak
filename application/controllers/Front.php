<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

 		$this->load->library('session');
		$this->load->helper('me');
		$this->load->model('M_data');
		$this->load->helper('tgl_indo');
		$this->load->helper(array('form', 'url'));
	}

	public function kembali()
	{
		return $this->input->server('HTTP_REFERER');
	}

	public function index()
	{	
		$data['judul'] 	= "Sistem Informasi Kependudukan";
		$this->load->view('frontEnd', $data, FALSE);
	}
}
