<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataRt extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {
            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');
            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

   
    function index()
    {
        $data['judul']   = "Data RT";
        $data['data']= $this->db->join('dusun', 'dusun.id_dusun = rt.dusun_id')
                                ->join('rw', 'rw.id_rw = rt.rw_id')
                                ->get('rt');
        $data['konten']  = $this->load->view('rt/main', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function formAdd()
    {
        $data['judul']      = "Tambah Data RT";
        $data['dusun']      = $this->db->get('dusun');
        $data['konten']     = $this->load->view('rt/formAdd', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function add()
    {
        $data = array(
            'nomor_rt' => $this->input->post('nomor_rt'),
            'dusun_id' => $this->input->post('dusun_id'),
            'rw_id' => $this->input->post('rw_id'),
            'nama_ketua_rt' => $this->input->post('nama_ketua'),
        );

        $cek = $this->db->where('rw_id', $this->input->post('rw_id'))->where('nomor_rt', $this->input->post('nomor_rt'))->get('rt')->row();

        if (empty($cek)) { 
            $this->M_data->create('rt', $data);
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-success" style="margin-top: 10px;">
                        <span><i class="fa fa-check"></i> Berhasil menambah data!</span>
                    </div>');

            redirect(base_url('data-rt'));
        }else{
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger" style="margin-top: 10px;">
                        <span><i class="fa fa-times"></i> RT sudah ada!</span>
                    </div>');

            redirect($this->kembali());
        }
    }

    function formEdit()
    {
        $data['judul']      = "Edit Data RT";
        $data['dusun']      = $this->db->get('dusun');
        $data['data']       = $this->db->where('id_rt', $this->uri->segment(2))->get('rt')->row();
        $data['konten']     = $this->load->view('rt/formUpdate', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function update()
    {   
        $data = array(
            'nomor_rt' => $this->input->post('nomor_rt'),
            'dusun_id' => $this->input->post('dusun_id'),
            'rw_id' => $this->input->post('rw_id'),
            'nama_ketua_rt' => $this->input->post('nama_ketua'),
        );

        $id = array('id_rt' => $this->input->post('id'));

        $this->M_data->ubah_data($id, $data,'rt');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Mengubah data!</span>
                </div>');

        redirect(base_url('data-rt'));
    }

    public function hapus()
    {
        $id = array('id_rt' => $this->uri->segment(2));
        $this->M_data->hapus_data($id,'rt');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Hapus Data Berhasil!</span>
                </div>');

        redirect( $this->kembali() );
    }

    public function getRw()
    {
        $data = $this->db->where('dusun_id', $this->input->post('id_dusun'))->get('rw')->result();

        if (!empty($data)) {
            $callback = ['status'=>'success', 'data'=>$data];
        }else{
            $callback = ['status'=>'error', 'data'=>""];
        }
        
        echo json_encode($callback);
    }

    public function getRt()
    {
        $data = $this->db->where('rw_id', $this->input->post('id_rw'))->get('rt')->result();

        if (!empty($data)) {
            $callback = ['status'=>'success', 'data'=>$data];
        }else{
            $callback = ['status'=>'error', 'data'=>""];
        }
        
        echo json_encode($callback);
    }

}
