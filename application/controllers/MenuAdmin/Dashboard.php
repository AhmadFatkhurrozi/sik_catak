<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');

            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }


    public function index()
    {
        $this->is__login();
        $data['judul']   = "Dashboard";
        $query = $this->db->select('COUNT(id) AS total, jk')->group_by('jk')->get('penduduk');

        // GRAFIK JK
        
        foreach($query->result_array() as $row)
        { 
          $data1[] = array(
               'name'     => $row['jk'],
               'y' => (int)$row['total']
            );    
        } 
        
        $data['jk'] = json_encode($data1);

        // GRAFIK PEKERJAAN

        $query2 =   $this->db->select('COUNT(id) AS total, pekerjaan')->group_by('pekerjaan')->get('penduduk');

        foreach($query2->result_array() as $a)
        { 
          $data2[] = array(
               'name'     => $a['pekerjaan'],
               'y' => (int)$a['total']
            );    
        }         

        $data['pekerjaan'] = json_encode($data2);

        // GRAFIK USIA

        $query3 =   $this->db->select('COUNT(id) AS total, YEAR(tgl_lahir) as tahun')->group_by('YEAR(tgl_lahir)')->get('penduduk');

        foreach($query3->result_array() as $b)
        { 
          $data3[] = array(
               'name'     => date('Y') - (int)$b['tahun']." Tahun",
               'y' => (int)$b['total']
            );    
        }         

        $data['usia'] = json_encode($data3);

        $data['konten']  = $this->load->view('dashboard', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

}
