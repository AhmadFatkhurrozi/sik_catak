<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataDusun extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {
            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');
            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

   
    function index()
    {
        $data['judul']   = "Data Dusun";
        $data['data']= $this->db->get('dusun');
        $data['konten']  = $this->load->view('dusun/main', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    public function kamus()
    {
        $data['judul']   = "Kamus ID RT, RW dan Dusun Catak Gayam";
        $data['data']= $this->db->join('rw', 'rw.dusun_id=dusun.id_dusun', 'both')
                                ->join('rt', 'rt.rw_id=rw.id_rw', 'both')->get('dusun');
        $data['konten']  = $this->load->view('dusun/kamus', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function formAdd()
    {
        $data['judul']      = "Tambah Data Dusun";
        $data['konten']     = $this->load->view('dusun/formAdd', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function add()
    {
        $data = array(
            'nama_dusun' => $this->input->post('nama_dusun'),
        );

        $cek = $this->db->where('nama_dusun', $this->input->post('nama_dusun'))->get('dusun')->row();

        if (empty($cek)) { 
            $this->M_data->create('dusun', $data);
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-success" style="margin-top: 10px;">
                        <span><i class="fa fa-check"></i> Berhasil menambah data!</span>
                    </div>');

            redirect(base_url('data-dusun'));
        }else{
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger" style="margin-top: 10px;">
                        <span><i class="fa fa-times"></i> Dusun sudah ada!</span>
                    </div>');

            redirect($this->kembali());
        }
    }

    function formEdit()
    {
        $data['judul']      = "Edit Data Dusun";
        $data['data']       = $this->db->where('id_dusun', $this->uri->segment(2))->get('dusun')->row();
        $data['konten']     = $this->load->view('dusun/formUpdate', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function update()
    {   
        $data = array(
            'nama_dusun'          => $this->input->post('nama_dusun'),
        );

        $id = array('id_dusun' => $this->input->post('id'));

        $this->M_data->ubah_data($id, $data,'dusun');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Mengubah data!</span>
                </div>');

        redirect(base_url('data-dusun'));
    }

    public function hapus()
    {
        $idDusun = array('id_dusun' => $this->uri->segment(2));
        $this->M_data->hapus_data($idDusun,'dusun');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Hapus Data Berhasil!</span>
                </div>');

        redirect( $this->kembali() );
    }

}
