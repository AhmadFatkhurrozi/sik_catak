<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataRw extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {
            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');
            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

   
    function index()
    {
        $data['judul']   = "Data RW";
        $data['data']= $this->db->join('dusun', 'dusun.id_dusun = rw.dusun_id')->get('rw');
        $data['konten']  = $this->load->view('rw/main', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function formAdd()
    {
        $data['judul']      = "Tambah Data RW";
        $data['dusun']      = $this->db->get('dusun');
        $data['konten']     = $this->load->view('rw/formAdd', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function add()
    {
        $data = array(
            'nomor_rw' => $this->input->post('nomor_rw'),
            'dusun_id' => $this->input->post('dusun_id'),
            'nama_ketua' => $this->input->post('nama_ketua'),
        );

        $cek = $this->db->where('dusun_id', $this->input->post('dusun_id'))->where('nomor_rw', $this->input->post('nomor_rw'))->get('rw')->row();

        if (empty($cek)) { 
            $this->M_data->create('rw', $data);
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-success" style="margin-top: 10px;">
                        <span><i class="fa fa-check"></i> Berhasil menambah data!</span>
                    </div>');

            redirect(base_url('data-rw'));
        }else{
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger" style="margin-top: 10px;">
                        <span><i class="fa fa-times"></i> RW sudah ada!</span>
                    </div>');

            redirect($this->kembali());
        }
    }

    function formEdit()
    {
        $data['judul']      = "Edit Data RW";
        $data['dusun']      = $this->db->get('dusun');
        $data['data']       = $this->db->where('id_rw', $this->uri->segment(2))->get('rw')->row();
        $data['konten']     = $this->load->view('rw/formUpdate', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function update()
    {   
        $data = array(
            'nomor_rw' => $this->input->post('nomor_rw'),
            'dusun_id' => $this->input->post('dusun_id'),
            'nama_ketua' => $this->input->post('nama_ketua'),
        );

        $id = array('id_rw' => $this->input->post('id'));

        $this->M_data->ubah_data($id, $data,'rw');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Mengubah data!</span>
                </div>');

        redirect(base_url('data-rw'));
    }

    public function hapus()
    {
        $id = array('id_rw' => $this->uri->segment(2));
        $this->M_data->hapus_data($id,'rw');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Hapus Data Berhasil!</span>
                </div>');

        redirect( $this->kembali() );
    }

}
