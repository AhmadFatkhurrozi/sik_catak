<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {
            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');
            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

   
    function index()
    {
        $data['judul']   = "Data Pengguna";
        $data['pengguna']= $this->db->get('users');
        $data['konten']  = $this->load->view('pengguna/main', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function formAdd()
    {
        $data['judul']      = "Tambah Data Pengguna";
        $data['konten']     = $this->load->view('pengguna/formAdd', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function add()
    {
        $data = array(
            'nama'          => $this->input->post('nama'),
            'username'      => $this->input->post('username'),
            'password'      => md5($this->input->post('password')),
            'level'         => $this->input->post('level'),
        );

        $cek = $this->db->where('username', $this->input->post('username'))->get('users')->row();

        if (empty($cek)) { 
            $this->M_data->create('users', $data);
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-success" style="margin-top: 10px;">
                        <span><i class="fa fa-check"></i> Berhasil menambah data!</span>
                    </div>');

            redirect(base_url('manage-pengguna'));
        }else{
            $this->session->set_flashdata('msg', 
                    '<div class="alert alert-danger" style="margin-top: 10px;">
                        <span><i class="fa fa-times"></i> Username telah digunakan!</span>
                    </div>');

            redirect($this->kembali());
        }
    }

    function formEdit()
    {
        $data['judul']          = "Edit Data Pengguna";
        $data['pengguna']       = $this->db->where('id', $this->uri->segment(2))->get('users')->row();
        $data['konten']         = $this->load->view('pengguna/formUpdate', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function update()
    {   
        $data = array(
            'nama'          => $this->input->post('nama'),
            'username'      => $this->input->post('username'),
            'password'      => md5($this->input->post('password')),
            'level'         => $this->input->post('level'),
        );

        $id = array('id' => $this->input->post('id'));

        $this->M_data->ubah_data($id, $data,'users');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Mengubah data!</span>
                </div>');

        redirect(base_url('manage-pengguna'));
    }

    public function hapus()
    {
        $idUser = array('id' => $this->uri->segment(2));
        $this->M_data->hapus_data($idUser,'users');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Hapus Data Berhasil!</span>
                </div>');

        redirect( $this->kembali() );
    }

}
