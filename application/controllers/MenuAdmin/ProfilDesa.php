<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilDesa extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {

            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');

            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }


    public function index()
    {
        $this->is__login();
        $data['judul']   = "Profil Desa";
        $data['profil']  = $this->db->where('id_profil_desa', 1)->get('profil_desa')->row();

        $data['konten']  = $this->load->view('admin/profil/main', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function formEdit()
    {
        $data['judul']          = "Edit Profil Desa";
        $data['profil']          = $this->db->where('id_profil_desa', $this->uri->segment(2))->get('profil_desa')->row();
        $data['konten']         = $this->load->view('admin/profil/edit', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    public function update()
    {
        $data = array(
            'kepala_desa'       => $this->input->post('kepala_desa'),
            'sekdes'            => $this->input->post('sekdes'),
            'kaur_keuangan'     => $this->input->post('kaur_keuangan'),
            'kaur_pemerintahan' => $this->input->post('kaur_pemerintahan'),
            'kaur_pembangunan'  => $this->input->post('kaur_pembangunan'),
            'kasi_kesra'        => $this->input->post('kasi_kesra'),
            'kaur_umum'         => $this->input->post('kaur_umum'),
            'kasun_utara'       => $this->input->post('kasun_utara'),
            'kasun_selatan'     => $this->input->post('kasun_selatan'),
            'kasun_tawangsari'  => $this->input->post('kasun_tawangsari'),
        );

        $id = array('id_profil_desa' => $this->input->post('id'));

        $this->M_data->ubah_data($id, $data,'profil_desa');

        $this->session->set_flashdata('msg', 
                '<div class="alert alert-success" style="margin-top: 10px;">
                    <span><i class="fa fa-check"></i> Berhasil Mengubah data!</span>
                </div>');

        redirect(base_url('profil-desa'));
    }
}
