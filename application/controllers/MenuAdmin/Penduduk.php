<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'third_party/PHPExcel/PHPExcel.php';
include_once APPPATH . 'third_party/Validation/Validation.php';

class Penduduk extends CI_Controller {

    public function __construct()

    {

        parent::__construct();

        $this->load->library('session');

        $this->load->helper('me');

        $this->load->model('M_data');

        $this->load->helper('tgl_indo');

        $this->load->helper(array('form', 'url'));

    }



    public function is__login()

    {

        if (empty($this->session->userdata('get_id'))) {

            $this->session->set_flashdata('msg', 

                '<p class="text-danger" style="margin-top: 10px;">

                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>

                </p');

            redirect(base_url('login'));

        }

    }



    public function kembali()

    {

        return $this->input->server('HTTP_REFERER');

    }



   

    function index()

    {

        $data['judul']   = "Data Penduduk";



        $query = $this->db->where('status', 'tinggal ditempat');

        if (!empty($this->input->get('usia'))) {

            // if ($this->input->get('usia') == "17") {

            //     $query->where(date('Y', strtotime('tgl_lahir')).'<=', (date('Y')-17));

            // }

        }



        $data['usia']= $this->input->get('usia');

        $data['data']= $query->get('penduduk');

        $data['konten']  = $this->load->view('admin/penduduk/main', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);

    }



    public function KepalaKeluarga()

    {

        $data['judul']   = "Data Kepala Keluarga";



        $query = $this->db->where('status_keluarga', 'Kepala Keluarga');



        $data['data']= $query->get('penduduk');

        $data['konten']  = $this->load->view('admin/penduduk/kk', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);

    }



    public function detailKeluarga()

    {

        $data['judul']          = "Detail Keluarga";

        $data['penduduk']       = $this->db->where('no_kk', $this->uri->segment(2))->get('penduduk');

        $data['konten']         = $this->load->view('admin/penduduk/detailKeluarga', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);

    }

    

    function formAdd()

    {

        $data['judul']      = "Tambah Data Penduduk";

        $data['dusun']      = $this->db->get('dusun');

        $data['konten']     = $this->load->view('admin/penduduk/formAdd', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);

    }

    public function import()
    {
        $data['judul']      = "Import Data Penduduk";

        $data['konten']     = $this->load->view('admin/penduduk/import', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);
    }

    public function importFile()
    {
        if (!empty($_FILES['file']['name'])) {

          @unlink('upload/import-data-penduduk.xlsx');

          // Set preference
          $config['upload_path'] = 'upload/';
          $config['allowed_types'] = 'xlsx';
          $config['max_size'] = '1024'; // max_size in kb
          $config['file_name'] = 'import-data-penduduk';

          //Load upload library
          $this->load->library('upload', $config);

          // File upload
          if ($this->upload->do_upload('file')) {
            // Get data about the file
            $uploadData = $this->upload->data();
          }
        }
    }

    public function doimportFile()
    {
        $validasi    = new Validation;
        $excelreader = new PHPExcel_Reader_Excel2007();

        $loadexcel = $excelreader->load('upload/import-data-penduduk.xlsx');

        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

        if ((count($sheet) - 1) > 3000) {
          echo '<div class="alert alert-warning"><ul><li>Data lebih dari 2000 baris</li></ul></div>';
        } else {

          // Array in table
          $arr_table_penduduk = [];
          $arr_table = $this->db->get('penduduk');
          $num2 = 1;
          foreach ($arr_table->result() as $r2) {
            $arr_table_penduduk[] = $r2->nama;
            $num2++;
          }

          // Array in file
          $arr_file_penduduk = [];

          $numrow = 1;
          foreach ($sheet as $row) {
            if ($numrow > 1) {
              $arr_file_penduduk[]          = $row['A']; // 

              $penduduk                     = $row['A'];

              $validasi->name('Nama penduduk baris ke - ' . ($numrow) . '')->value($penduduk)->required();
            }
            $numrow++;
          }


          if ($validasi->isSuccess()) {
            $count_array = array_count_values($arr_file_penduduk);
            $error = 0;
            $object = [];
            $numrow2 = 1;
            $error_msg = '<div class="alert alert-warning"><ul>';
            foreach ($sheet as $row) {

              if ($numrow2 > 1) {

                $nama                      = $row['A'];
                $no_kk                     = $row['B'];
                $nik                       = $row['C'];
                $jk                        = ($row['D'] == "L") ? "Laki-laki": "Perempuan";
                $tgl_lahir                 = date('Y-m-d', strtotime($row['E']));
                $tempat_lahir              = $row['F'];
                $agama                     = $row['G'];
                $pekerjaan                 = $row['H'];
                $status_kawin              = $row['I'];
                $status_keluarga           = $row['J'];
                $kwarganegaraan            = $row['K'];
                $nama_ayah                 = $row['L'];
                $nama_ibu                  = $row['M'];
                $dusun_id                  = $row['N'];
                $dusun                     = $row['O'];
                $rw_id                     = $row['P'];
                $rw                        = $row['Q'];
                $rt_id                     = $row['R'];
                $rt                        = $row['S'];
                $status                    = $row['T'];

                $object[] = [
                    'nama'          => $nama,
                    'nik'           => $nik,
                    'no_kk'         => $no_kk,
                    'jk'            => $jk,
                    'tgl_lahir'     => $tgl_lahir,
                    'tempat_lahir'  => $tempat_lahir,
                    'agama'         => $agama,
                    'pekerjaan'     => $pekerjaan,
                    'status_kawin'  => $status_kawin,
                    'status_keluarga'=> $status_keluarga,
                    'kwarganegaraan'=> $kwarganegaraan,
                    'nama_ayah'     => $nama_ayah,
                    'nama_ibu'      => $nama_ibu,
                    'dusun_id'      => $dusun_id,
                    'dusun'         => $dusun,
                    'rw_id'         => $rw_id,
                    'rw'            => $rw,
                    'rt_id'         => $rt_id,
                    'rt'            => $rt,
                    'status'        => $status,
                ];



                if ($count_array[$penduduk]  > 1) {
                  $error_msg .= '<li>nama penduduk baris ke - ' . ($numrow2) . ' ganda</li>';
                  $error++;
                }

                if (in_array($penduduk, $arr_table_penduduk)) {
                  $error_msg .= '<li>nama penduduk baris ke - ' . ($numrow2) . ' sudah ada</li>';
                  $error++;
                }
              }
              $numrow2++;
            }
            $error_msg .= '</ul></div>';

            if ($error == 0) {
              $res = $this->db->insert_batch('penduduk', $object);

              if ($res) {
                echo '<div class="alert alert-success">';
                echo count($sheet) . ' data. Berhasil di import ';
                echo '<a href="' . base_url() . 'data-penduduk">Lihat Data</a>';
                echo '</div>';
              } else {
                echo '<div class="alert alert-warning">';
                echo 'Terjadi masalah saat mengimport, silahkan coba beberapa saat lagi';
                echo '</div>';
              }
            } else {
              echo $error_msg;
            }
          } else {
            echo '<div class="alert alert-warning">';
            echo $validasi->displayErrors();
            echo '</div>';
          }
        }
    }


    function add()

    {

        $data = array(

            'nama'          => $this->input->post('nama'),

            'nik'           => $this->input->post('nik'),

            'no_kk'           => $this->input->post('no_kk'),

            'jk'            => $this->input->post('jk'),

            'tgl_lahir'     => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),

            'tempat_lahir'  => $this->input->post('tempat_lahir'),

            'agama'         => $this->input->post('agama'),

            'pekerjaan'         => $this->input->post('pekerjaan'),

            'status_kawin'  => $this->input->post('status_kawin'),

            'status_keluarga'=> $this->input->post('status_keluarga'),

            'kwarganegaraan'=> $this->input->post('warga'),

            'nama_ayah'     => $this->input->post('nama_ayah'),

            'nama_ibu'      => $this->input->post('nama_ibu'),

            'dusun_id'      => $this->input->post('dusun_id'),

            'dusun'         => $this->db->where('id_dusun', $this->input->post('dusun_id'))->get('dusun')->row()->nama_dusun,

            'rw_id'         => $this->input->post('rw_id'),

            'rw'            => $this->db->where('id_rw', $this->input->post('rw_id'))->get('rw')->row()->nomor_rw,

            'rt_id'         => $this->input->post('rt_id'),

            'rt'            => $this->db->where('id_rt', $this->input->post('rt_id'))->get('rt')->row()->nomor_rt,

            'status'        => 'tinggal ditempat',

        );



        $cekNik = strlen($this->input->post('nik'));



        if ($cekNik == 16) {

            $this->M_data->create('penduduk', $data);

            $this->session->set_flashdata('msg', 

                    '<div class="alert alert-success" style="margin-top: 10px;">

                        <span><i class="fa fa-check"></i> Berhasil menambah data!</span>

                    </div>');



            redirect(base_url('data-penduduk'));

        }else{

            $this->session->set_flashdata('msg', 

                    '<div class="alert alert-danger" style="margin-top: 10px;">

                        <span><i class="fa fa-times"></i> NIK harus 16 angka!</span>

                    </div>');



            redirect($this->kembali());

        }

    }



    function formEdit()

    {

        $data['judul']          = "Edit Data Penduduk";

        $data['penduduk']       = $this->db->where('id', $this->uri->segment(2))->get('penduduk')->row();

        $data['rw']             = $this->db->where('nomor_rw',  $data['penduduk']->rw)->get('rw')->row()->id_rw;

        $data['rt']             = $this->db->where('rw_id',  $data['rw'])->where('nomor_rt', $data['penduduk']->rt)->get('rt')->row()->id_rt;

        $data['dusun']          = $this->db->get('dusun');

        $data['konten']         = $this->load->view('admin/penduduk/formUpdate', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);

    }



    function update()

    {   

        $data = array(

            'nama'          => $this->input->post('nama'),

            'nik'           => $this->input->post('nik'),

            'no_kk'           => $this->input->post('no_kk'),

            'jk'            => $this->input->post('jk'),

            'tgl_lahir'     => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),

            'tempat_lahir'  => $this->input->post('tempat_lahir'),

            'agama'         => $this->input->post('agama'),

            'pekerjaan'         => $this->input->post('pekerjaan'),

            'status_kawin'  => $this->input->post('status_kawin'),

            'status_keluarga'=> $this->input->post('status_keluarga'),

            'kwarganegaraan'=> $this->input->post('warga'),

            'nama_ayah'     => $this->input->post('nama_ayah'),

            'nama_ibu'      => $this->input->post('nama_ibu'),

            'dusun_id'      => $this->input->post('dusun_id'),

            'dusun'         => $this->db->where('id_dusun', $this->input->post('dusun_id'))->get('dusun')->row()->nama_dusun,

            'rw_id'         => $this->input->post('rw_id'),

            'rw'            => $this->db->where('id_rw', $this->input->post('rw_id'))->get('rw')->row()->nomor_rw,

            'rt_id'         => $this->input->post('rt_id'),

            'rt'            => $this->db->where('id_rt', $this->input->post('rt_id'))->get('rt')->row()->nomor_rt,

        );



        $id = array('id' => $this->input->post('id'));



        $this->M_data->ubah_data($id, $data,'penduduk');



        $this->session->set_flashdata('msg', 

                '<div class="alert alert-success" style="margin-top: 10px;">

                    <span><i class="fa fa-check"></i> Berhasil Mengubah data!</span>

                </div>');



        redirect(base_url('data-penduduk'));

    }



    public function hapus($id)

    {

        $idUser = array('id' => $id);

        $this->M_data->hapus_data($idUser,'penduduk');



        $this->session->set_flashdata('msg', 

                '<div class="alert alert-success" style="margin-top: 10px;">

                    <span><i class="fa fa-check"></i> Hapus Data Berhasil!</span>

                </div>');



        redirect( $this->kembali() );

    }



    function detail()

    {

        $data['judul']          = "Detail Penduduk";

        $data['penduduk']       = $this->db->where('id', $this->uri->segment(2))->get('penduduk')->row();

        $data['konten']         = $this->load->view('admin/penduduk/detail', $data, TRUE);



        $this->load->view('layout/master', $data, FALSE);

    }



    public function getDataPenduduk()

    {

        $data = $this->db->where('nik', $this->input->post('nik'))->get('penduduk')->row();



        if (!empty($data)) {

            $callback = ['status'=>'success', 'data'=>$data];

        }else{

            $callback = ['status'=>'error', 'data'=>""];

        }

        

        echo json_encode($callback);

    }

}

