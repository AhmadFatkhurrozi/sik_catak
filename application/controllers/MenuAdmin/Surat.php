<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function is__login()
    {
        if (empty($this->session->userdata('get_id'))) {
            $this->session->set_flashdata('msg', 
                '<p class="text-danger" style="margin-top: 10px;">
                    <span><i class="fa fa-times"></i>Silahkan Login terlebih dahulu</span>
                </p');
            redirect(base_url('login'));
        }
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

   
    function kematian()
    {
        $data['judul']   = "Surat Kematian";
        $data['data']= $this->db->join('penduduk as p', 'p.nik=surat_kematian.nik_jenazah')
                                ->get('surat_kematian');

        $data['konten']  = $this->load->view('admin/surat/kematian', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

     function skck()
    {
        $data['judul']   = "SKCK";
        $data['data']= $this->db->select('p.*, skck.keterangan, skck.tgl_dibuat')
                                ->join('penduduk as p', 'p.nik=skck.nik')
                                ->get('skck');

        $data['konten']  = $this->load->view('admin/surat/skck', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function sktm()
    {
        $data['judul']   = "Surat Ketarangan Tidak Mampu";
        $data['data']= $this->db->select('p.*, sktm.keterangan, sktm.tgl_dibuat')
                                ->join('penduduk as p', 'p.nik=sktm.nik')
                                ->get('sktm');

        $data['konten']  = $this->load->view('admin/surat/sktm', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function nikah()
    {
        $data['judul']   = "Pengantar Nikah";
        $data['data']= $this->db->select('p.*, pengantar_nikah.no_surat, pengantar_nikah.tgl_dibuat')
                                ->join('penduduk as p', 'p.nik=pengantar_nikah.nik')
                                ->get('pengantar_nikah');

        $data['konten']  = $this->load->view('admin/surat/nikah', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }

    function kelahiran()
    {
        $data['judul']   = "Surat Kelahiran";
        $data['data']= $this->db->get('surat_kelahiran');

        $data['konten']  = $this->load->view('admin/surat/kelahiran', $data, TRUE);

        $this->load->view('layout/master', $data, FALSE);
    }


}
