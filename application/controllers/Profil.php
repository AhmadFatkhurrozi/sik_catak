<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('me');
        $this->load->model('M_data');
        $this->load->helper('tgl_indo');
        $this->load->helper(array('form', 'url'));
    }

    public function kembali()
    {
        return $this->input->server('HTTP_REFERER');
    }

    public function index()
    {   
        $data['judul']  = "Profil Desa Catak Gayam";

        $content = $this->load->view('profil/main', $data, TRUE);

        $callback = ['status'=>'success', 'content'=>$content];
        echo json_encode($callback);
    }
}
