<div class="col-12">
    <main>
      <h1 class="text-light p-3" style="font-size: 32px;"><?=$judul;?></h1>
      <hr class="clearfix py-3">
      
      <div class="container">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </main>

     <a href="javascript:void(0)" class="btn-cancel pull-right btn btn-success btn-sm m-5"><span class="fa fa-chevron-left"></span> Kembali</a>
</div>

<script type="text/javascript">
    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });
</script>