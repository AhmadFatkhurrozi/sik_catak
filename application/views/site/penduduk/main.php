<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid mt-4">
   <div class="row">
     <div class="col-lg-3 col-md-3 col-sm-12 col-12">
        <fieldset class="form-group">
           <select class="form-control" id="dusun_id" onchange="getRw(this)">
             <option disabled="" selected="">.:: Pilih Dusun ::.</option>
             <?php foreach ($dusun->result() as $key) { ?>
                <option value="<?=$key->id_dusun?>"><?=$key->nama_dusun;?></option>
             <?php } ?>
           </select>
         </fieldset>
      </div>

     <div class="col-lg-3 col-md-3 col-sm-12 col-12">
         <fieldset class="form-group">
            <select class="form-control" name="rw_id" id="rw_id" onchange="getRt(this)">
            </select>
         </fieldset>
     </div>

     <div class="col-lg-3 col-md-3 col-sm-12 col-12">
         <fieldset class="form-group">
            <select class="form-control" name="rt_id" id="rt_id" onchange="getRt(this)">
            </select>
         </fieldset>
     </div>

     <div class="col-lg-3 col-md-3 col-sm-12 col-12">
         <a href="javascript:void(0)" class="btn btn-success btn-block text-light" onclick="cariPenduduk()">Cari</a>
     </div>
   </div>
   <hr>
   <div class="row">
      <div class="col-lg-12 col-md-12">
        <div style="overflow: auto;height: 500px; margin-bottom: 40px;">
            <table class="table table-bordered table-hover table-sm" id="datatable">
                <thead class="bg-primary text-light">
                   <tr>
                       <th width="35px">No</th>
                       <th>NIK</th>
                       <th>Nama</th>
                       <th>RT</th>
                       <th>RW</th>
                       <th>Dusun</th>
                       <th>Status</th>
                   </tr>
                </thead>
                <tbody id="hasil" style="height: 400px;width: 100%;overflow: auto;">
                    <tr>
                    <td style="padding: 5px;" colspan="7" align="center"> .:: pilih data untuk ditampilkan ::. </td>
                    </tr>
                </tbody>
            </table>
        </div>
      </div>

      <div class="col-md-12">
            <a href="<?=base_url();?>" class="btn btn-success pull-right">Kembali</a>
        </div>
   </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        // $('#datatable').DataTable();
    } );

   function getRw(ini){
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('get-rw')?>',
            async : true,
            dataType : 'json',
            data: {id_dusun:ini.value},
            success: function(response) { 
                if(response.status == 'success'){
                  var html = '<option value="">.:: Nomor RW ::.</option>';
                  $.each(response.data,function(k,v){
                    html += '<option value="'+v.id_rw+'">'+v.nomor_rw+'</option>';
                  });
                  $('#rw_id').html(html);
                  // $('#rw_id').trigger("chosen:updated");
                } else {
                  return "";
                }
            }
        });
    } 

    function getRt(ini){
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('get-rt')?>',
            async : true,
            dataType : 'json',
            data: {id_rw:ini.value},
            success: function(response) { 
                if(response.status == 'success'){
                  var html = '<option value="">.:: Nomor RT ::.</option>';
                  $.each(response.data,function(k,v){
                    html += '<option value="'+v.id_rt+'">'+v.nomor_rt+'</option>';
                  });
                  $('#rt_id').html(html);
                  // $('#rw_id').trigger("chosen:updated");
                } else {
                  return "";
                }
            }
        });
    } 

    function cariPenduduk() {
      var dusun_id = $('#dusun_id').val();
      var rw_id = $('#rw_id').val();
      var rt_id = $('#rt_id').val();
      $('#hasil').html("");
       $.ajax({
            type: 'POST',
            url: '<?php echo site_url('cari-penduduk')?>',
            async : true,
            dataType : 'json',
            data: {dusun_id:dusun_id, rw_id:rw_id, rt_id:rt_id},
            success: function(response) { 
                var tag = '';
                if(response.status == 'success'){
                    var no = 0;
                    $.each(response.data, function(k,h){
                        var nomer = 1 + no++;
                        tag += '<tr>';
                        tag += '<td style="padding: 5px;" align="center" valign="top">'+nomer+'</td>';
                        tag += '<td style="padding: 5px;" align="center" valign="top">'+h.nik+'</td>';
                        tag += '<td style="padding: 5px;" align="left" valign="top">'+h.nama+'</td>';
                        tag += '<td style="padding: 5px;" align="center" valign="top">'+h.rt+'</td>';
                        tag += '<td style="padding: 5px;" align="center" valign="top">'+h.rw+'</td>';
                        tag += '<td style="padding: 5px;" align="center" valign="top">'+h.dusun+'</td>';
                        if (h.status == "meninggal") {
                            var stts = 'danger';
                        }else if(h.status == "pindah"){
                            var stts = 'info';
                        }else{
                            var stts = 'success';
                        }
                        tag += '<td style="padding: 5px;" align="center" valign="top"><span class="badge badge-'+stts+'">'+h.status+'</span></td>';
                        tag += '</tr>';
                    });
                    no = no + response.data.length;

                }else{
                    tag += '<tr>';
                    tag += '<td style="padding: 5px;" colspan="7" align="center"> .:: data tidak ditemukan ::. </td>';
                    tag += '</tr>';
                }
                $('#hasil').append(tag);
            }
        });
    }
</script>
