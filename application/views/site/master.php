<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Kependudukan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php 
        echo __css('fontawesome');
        echo __css('bootstrap');
        echo __css('fontastic');
        echo __css('default');
        echo __css('custom');
      echo __js('jquery');
      echo __js('popper'); 
      echo __js('bootstrap');
      echo __js('checkall');
      echo __js('_checkall');
     ?>
     <link rel="stylesheet" type="text/css" href="<?=base_url('/dist/datatable/dataTables.bootstrap4.min.css')?>">
    <script src="<?=base_url('/dist/datatable/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('/dist/datatable/dataTables.bootstrap4.min.js')?>"></script>
    <script src="<?=base_url('/dist/js/highcharts.js')?>"></script>
    
    <style type="text/css">
      .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: #fff;
      }
      .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
        font: 14px arial;
      }
      ul li a {
        color: #000 !important;
      }
      nav.side-navbar{
        /*background-color: rgb(0 0 0 / 68%) !important;*/
      }
      table tbody tr td{
        color: #000 !important;
      }
      .list-group-item {
        border: 0 !important;
      }

      html, body {
         margin:0;
         padding:0;
         height:100%;
      }

      .page{
        position:relative;
        min-height: 100%;
      }

      .highcharts-credits{
        display: none !important;
      }
    </style>

  </head>
  <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <a class="navbar-brand px-3" href="<?=base_url('/')?>">SIK Catak Gayam</a>
    </nav>

    <div class="page">
      <?php echo $konten; ?>

      <footer class="main-footer" style="bottom: 0 !important;">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 text-right">
              <p>Novi &copy; 2020</p>
            </div>
          </div>
        </div>
      </footer>
    </div>

   <script>
    $(document).ready(function(){
    $(".preloader").fadeOut();
    })
   </script>

  </body>
</html>