<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body mb-5">
         <div class="row" style="padding: 10px 0px;">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div id="grafikPekerjaan"></div>
            </div>
        </div>
        <div class="row" style="padding: 10px 0px;">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div id="container"></div>
            </div>
        
            <div class="col-lg-9 col-md-9 col-sm-12">
                <div id="grafikUsia"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?=base_url();?>" class="btn btn-success pull-right">Kembali</a>
            </div>
        </div>
    </div>
    <br>
</div>

<script type="text/javascript">
     Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                    [0, color],
                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        })
    });

    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Statistik Penduduk Berdasarkan Jenis Kelamin'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f} jiwa</b>'
        },
        accessibility: {
            point: {
                valueSuffix: ' jiwa'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: <?=$jk;?>
        }]
    });   

    Highcharts.chart('grafikPekerjaan', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Statistik Penduduk Berdasarkan Pekerjaan'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f} jiwa</b>'
        },
        accessibility: {
            point: {
                valueSuffix: 'jiwa'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.0f} jiwa '
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: <?=$pekerjaan;?>
        }]
    });   

    Highcharts.chart('grafikUsia', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Statistik Penduduk Berdasarkan Usia'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f} jiwa</b>'
        },
        accessibility: {
            point: {
                valueSuffix: 'jiwa'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.0f} jiwa '
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: <?=$usia;?>
        }]
    });    
</script>

