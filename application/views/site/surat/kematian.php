<style type="text/css">
   .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
</style>
<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="">
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-12 mb-5 py-3 card">
              <div class="container card-body">
                <form action="<?=base_url('simpan-kematian')?>" method="POST">
                <div class="row">
                  <div class="col-md-6">
                    <label class="text-center">Pelapor</label>
                    <hr>

                    <fieldset class="form-group">
                        <label>Nama Pelapor</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_pelapor" readonly="" id="nama_pelapor" value="<?=$this->session->userdata('penduduk_nama');?>">
                    </fieldset>
                  </div>
                  
                  <div class="col-md-6">
                    <label class="text-center">Data Orang yang Meninggal</label>
                    <hr>
                    <fieldset class="form-group">
                        <label>nik</label>
                        <input type="text" autocomplete="off" onchange="getDataPenduduk()" class="form-control" name="nik" placeholder="masukkan nik" id="nik">
                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Nama</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama" readonly="" id="nama">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" readonly="" id="alamat"></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Hari</label>
                        <select name="hari" class="form-control">
                          <option disabled="" selected="">.:: pilih hari ::.</option>
                          <option value="Senin">Senin</option>
                          <option value="Selasa">Selasa</option>
                          <option value="Rabu">Rabu</option>
                          <option value="Kamis">Kamis</option>
                          <option value="Jumat">Jumat</option>
                          <option value="Sabtu">Sabtu</option>
                          <option value="Minggu">Minggu</option>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tanggal</label>
                        <input type="date" autocomplete="off" class="form-control" name="tanggal">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tempat Meninggal</label>
                        <input type="text" autocomplete="off" class="form-control" name="tempat">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Sebab Meninggal</label>
                        <input type="text" autocomplete="off" class="form-control" name="sebab">
                    </fieldset>

                    <div class="float-right">
                      <a href="<?=base_url('pembuatan-surat')?>" class="btn btn-info">Kembali</a>
                      <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                  </div>

                </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
  function getDataPenduduk() {
    var nik = $('#nik').val();
    $.ajax({
            type: 'POST',
            url: '<?php echo site_url('getDataPenduduk')?>',
            async : true,
            dataType : 'json',
            data: {nik:nik},
            success: function(response) { 
                if(response.status == 'success'){
                  $('#nama').val(response.data.nama);
                  var alm = "Dsn. "+response.data.dusun+", RT. "+response.data.rt+", RW. "+response.data.rw+" Desa Catakgayam, Kecamatan   Mojowarno, Kabupaten Jombang";
                  $('#alamat').val(alm);
                } else {
                  $('#notifications').html('nik tidak ditemukan');
                }
            }
        });
  }
</script>