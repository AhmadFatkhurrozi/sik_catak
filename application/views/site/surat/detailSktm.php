<style type="text/css">
   .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
</style>
<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="">
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-12 mb-5 pt-3">
              <a href="<?=base_url('cetak-sktm/'.$data->nik);?>" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak</a>
          </div>
          <div class="col-md-12 mb-5">
              <div class="container">
                <form action="" method="POST">
                <div class="row">
                  <div class="col-md-6">
                  
                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">NIK</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->nik;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->nama;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Alamat</label>
                        <span class="col-md-7  col-sm-6 col-6">: Dsn. <?=$data->dusun;?>, RT. <?=$data->rt;?>, RW. <?=$data->rw;?>  Desa Catakgayam, Kecamatan Mojowarno, Kabupaten Jombang</span>
                    </fieldset>

                  </div>

                  <div class="col-md-6">
                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Agama</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->agama;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Pekerjaan</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->pekerjaan;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Tujuan Pembuatan Surat Ket. Tidak Mampu</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->keterangan;?></span>
                    </fieldset>
                  </div>
                </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
  
</script>