<style type="text/css">
   .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
</style>
<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="">
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-12 mb-5 py-3 card">
              <div class="container card-body">
                <form action="<?=base_url('simpan-surat-kelahiran')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-md-6">
                    <fieldset class="form-group">
                        <label>Nama Anak</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_anak" id="nama_anak">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" autocomplete="off" class="form-control" name="tmpt_lahir" id="tmpt_lahir">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" autocomplete="off" class="form-control" name="tgl_lahir" id="tgl_lahir">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Agama</label>
                        <select class="form-control" name="agama">
                            <option selected="" value="" disabled=""> .:: Pilih Agama ::.</option>
                            <option value="Islam">Islam</option>
                            <option value="Kristen Protestan">Kristen Protestan</option>
                            <option value="Kristen Katolik">Kristen Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Buddha">Buddha</option>
                            <option value="Khonghucu">Khonghucu</option>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Jenis Kelamin</label>
                        <label class="radio-inline mx-3">
                            <input type="radio" name="jk" value="Laki-laki"> Laki-laki
                        </label>
                        <label class="radio-inline mx-3">
                            <input type="radio" name="jk" value="Perempuan"> Perempuan
                        </label>
                    </fieldset>
                  
                    <fieldset class="form-group">
                        <label>Anak ke-</label>
                        <input type="number" autocomplete="off" class="form-control" name="no_urut" id="no_urut">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Foto KMS</label>
                        <input type="file" autocomplete="off" class="form-control" name="foto_kms" id="foto_kms">
                    </fieldset>
                  </div>

                  <div class="col-md-6">
                     <fieldset class="form-group">
                        <label>NIK Ibu</label>
                        <input type="text" autocomplete="off" onchange="getDataPendudukIbu()" class="form-control" name="nik_ibu" placeholder="masukkan nik ibu" id="nik_ibu">
                        <p id="notifications2"></p>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Nama Ibu</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_ibu" id="nama_ibu">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Umur Ibu</label>
                        <input type="number" autocomplete="off" class="form-control" name="umur_ibu" id="umur_ibu">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>NIK Ayah</label>
                        <input type="text" autocomplete="off" onchange="getDataPendudukAyah()" class="form-control" name="nik_ayah" placeholder="masukkan nik ayah" id="nik_ayah">
                        <p id="notifications1"></p>
                    </fieldset>

                     <fieldset class="form-group">
                        <label>Nama Ayah</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_ayah" id="nama_ayah">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Umur Ayah</label>
                        <input type="number" autocomplete="off" class="form-control" name="umur_ayah" id="umur_ayah">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" id="alamat_ayah" name="alamat"></textarea>
                    </fieldset>

                    <div class="float-right">
                        <a href="<?=base_url('pembuatan-surat')?>" class="btn btn-info">Kembali</a>
                        <button type="submit" class="btn btn-success">Simpan</button>
                    </div>
                  </div>

                </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
 function getDataPendudukAyah() {
    var nik = $('#nik_ayah').val();
    $.ajax({
            type: 'POST',
            url: '<?php echo site_url('getDataPenduduk')?>',
            async : true,
            dataType : 'json',
            data: {nik:nik},
            success: function(response) { 
                if(response.status == 'success'){
                  $('#notifications1').html('');
                  $('#nama_ayah').val(response.data.nama);
                  var alm = "Dsn. "+response.data.dusun+", RT. "+response.data.rt+", RW. "+response.data.rw+" Desa Catakgayam, Kecamatan   Mojowarno, Kabupaten Jombang";
                  $('#alamat_ayah').val(alm);
                  getAgeAyah(response.data.tgl_lahir)
                } else {
                  $('#notifications1').html('nik tidak ditemukan');
                  $('#nama_ayah').val('');
                  $('#alamat_ayah').val('');
                }
            }
        });
  }

  function getDataPendudukIbu() {
    var nik = $('#nik_ibu').val();
    $.ajax({
            type: 'POST',
            url: '<?php echo site_url('getDataPenduduk')?>',
            async : true,
            dataType : 'json',
            data: {nik:nik},
            success: function(response) { 
                if(response.status == 'success'){
                  $('#notifications2').html('');
                  $('#nama_ibu').val(response.data.nama);
                  getAgeIbu(response.data.tgl_lahir)
                } else {
                  $('#notifications2').html('nik tidak ditemukan');
                  $('#nama_ibu').val('');
                }
            }
        });
  }

  function getAgeAyah(date) {
        var today = new Date();
        var birthday = new Date(date);
        var year = 0;
        if (today.getMonth() < birthday.getMonth()) {
            year = 1;
        } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
            year = 1;
        }
        var age = today.getFullYear() - birthday.getFullYear() - year;

        if(age < 0){
            age = 0;
        }
        $('#umur_ayah').val(age);
  }

  function getAgeIbu(date) {
        var today = new Date();
        var birthday = new Date(date);
        var year = 0;
        if (today.getMonth() < birthday.getMonth()) {
            year = 1;
        } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
            year = 1;
        }
        var age = today.getFullYear() - birthday.getFullYear() - year;

        if(age < 0){
            age = 0;
        }
        $('#umur_ibu').val(age);
  }
</script>