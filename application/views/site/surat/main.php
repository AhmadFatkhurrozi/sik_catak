<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="" style="padding-top: 60px;">
        <div class="row features-small wow fadeIn">
          <div class="col-md-12 text-center">
            <h3>Masukkan NIK</h3>
          </div>
        </div>
        
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-4 offset-md-4 mb-5 py-3">
            <form class="form-login" method="POST" action="<?=base_url('cek-nik')?>">
              <div class="md-form input-group pl-0 mb-2">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i> </span>
                </div>
                <input type="text" class="form-control" placeholder="NIK" name="nik" aria-describedby="basic-addon1">
              </div>
              <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>

              <div class="form-group text-center">
                <a href="<?=base_url();?>" class="btn btn-success">Kembali</a>
                <button type="submit" class="btn lt-register-btn btn-login btn-primary">Lanjutkan</button>
              </div>
            </form>
              </div>
        </div>
      </div>
    </section>
</div>
