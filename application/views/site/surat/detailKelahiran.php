<style type="text/css">
   .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
</style>
<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="">
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-12 mb-5 pt-3">
              <a href="<?=base_url('cetak-surat-kelahiran/'.$data->id_kelahiran);?>" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak</a>
          </div>
          <div class="col-md-12 mb-5">
              <div class="container">
                <form action="" method="POST">
                <div class="row">
                  <div class="col-md-6">

                    <fieldset class="form-group row">
                      <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama Anak</label>
                      <span class="col-md-7  col-sm-6 col-6">: <?=$data->nama_anak;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Tempat Lahir</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->tmpt_lahir;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Tanggal Lahir</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=date('d-m-Y', strtotime($data->tgl_lahir));?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Agama</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->agama;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Jenis Kelamin</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->jk;?></span>
                    </fieldset>
                  
                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Anak ke-</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->no_urut;?></span>
                    </fieldset>
                  </div>

                  <div class="col-md-6">
                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama Ibu</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->nama_ibu;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Umur Ibu</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->umur_ibu;?></span>
                    </fieldset>

                     <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama Ayah</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->nama_ayah;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Umur Ayah</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->umur_ayah;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Alamat</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->alamat;?></span>
                    </fieldset>

                  </div>
                  <hr>
                  <div class="col-md-12">
                    <h4 class="text-center">KMS</h4>
                    <center>
                        <img src="<?=base_url('/upload/'.$data->foto_kms)?>" width="70%">
                    </center>
                    </div>
                </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
  
</script>