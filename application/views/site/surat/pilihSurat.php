<style type="text/css">
   .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
</style>
<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="" style="padding-top: 60px;">
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-12 mb-5 py-3">
              <div class="container">
                <div class="row">
                  <div class="col-md-4">
                    <div class="card text-center mx-3">
                      <a href="<?=base_url('surat-kematian');?>" class="pilihan">
                        <i class="fa fa-envelope fa-3x p-2"></i>
                        <div class="card-body">
                          <h4 class="card-title text-dark">Surat Kematian</h4>
                        </div>
                      </a>
                    </div>
                  </div>
                  
                  <div class="col-md-4">
                    <div class="card text-center mx-3">
                      <a href="<?=base_url('surat-kelahiran');?>" class="pilihan">
                        <i class="fa fa-envelope fa-3x p-2"></i>
                        <div class="card-body">
                          <h4 class="card-title text-dark">Surat Kelahiran</h4>
                        </div>
                      </a>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="card text-center mx-3">
                      <a href="<?=base_url('surat-nikah');?>" class="pilihan">
                        <i class="fa fa-envelope fa-3x p-2"></i>
                        <div class="card-body">
                          <h4 class="card-title text-dark">Surat Pengantar Nikah</h4>
                        </div>
                      </a>
                    </div>
                  </div>

                 <!--  <div class="col-md-4">
                    <div class="card text-center mx-3">
                      <a href="<?=base_url('surat-pindah');?>" class="pilihan">
                        <i class="fa fa-envelope fa-3x p-2"></i>
                        <div class="card-body">
                          <h4 class="card-title text-dark">Surat Perpindahan</h4>
                        </div>
                      </a>
                    </div>
                  </div> -->

                  <div class="col-md-6">
                    <div class="card text-center mx-3">
                      <a href="<?=base_url('sktm');?>" class="pilihan">
                        <i class="fa fa-envelope fa-3x p-2"></i>
                        <div class="card-body">
                          <h4 class="card-title text-dark">Surat Keterangan Tidak Mampu</h4>
                        </div>
                      </a>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="card text-center mx-3">
                      <a href="<?=base_url('skck');?>" class="pilihan">
                        <i class="fa fa-envelope fa-3x p-2"></i>
                        <div class="card-body">
                          <h4 class="card-title text-dark">SKCK</h4>
                        </div>
                      </a>
                    </div>
                  </div>

                </div>
                
                <div class="pt-5">
                  <a href="<?=base_url('/')?>" class="btn btn-info float-right">Kembali</a>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
</div>
