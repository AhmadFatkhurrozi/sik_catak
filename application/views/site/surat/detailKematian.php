<style type="text/css">
   .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/header.png');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
</style>
<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom text-center"><?php echo $judul; ?></h2>
    </div>
</header>
<div class="container-fluid">
   <section>
      <div class="">
        <div class="row features-small mt-1 wow fadeIn">
          <div class="col-md-12 mb-5 pt-3">
              <a href="<?=base_url('cetak-kematian/'.$data->nik);?>" class="btn btn-danger pull-right"><i class="fa fa-print"></i> Cetak</a>
          </div>
          <div class="col-md-12 mb-5">
              <div class="container">
                <form action="<?=base_url('simpan-kematian')?>" method="POST">
                <div class="row">
                  <div class="col-md-6">
                    <label class="text-center">Data Orang yang Meninggal</label>
                    <hr>
                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">NIK</label>
                        <span class="col-md-7  col-sm-6 col-6">: <?=$data->nik_jenazah;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->nama;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Alamat</label>
                        <span class="col-md-7  col-sm-6 col-6">: Dsn. <?=$data->dusun;?>, RT. <?=$data->rt;?>, RW. <?=$data->rw;?>  Desa Catakgayam, Kecamatan Mojowarno, Kabupaten Jombang</span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Hari Meninggal</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->hari_meninggal;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=date('d-m-Y', strtotime($data->tgl_meninggal));?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Tempat Meninggal</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->tmpt_meninggal;?></span>
                    </fieldset>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Sebab Meninggal</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->sebab_meninggal;?></span>
                    </fieldset>

                  </div>

                  <div class="col-md-6">
                    <label class="text-center">Pelapor</label>
                    <hr>

                    <fieldset class="form-group row">
                        <label class="col-md-5 col-sm-6 col-6 font-weight-bold">Nama Pelapor</label>
                       <span class="col-md-7  col-sm-6 col-6">: <?=$data->nama_pelapor;?></span>
                    </fieldset>
                  </div>
                </div>
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
  
</script>