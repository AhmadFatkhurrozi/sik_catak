<!DOCTYPE html>
<html>
<head>
    <title><?=$judul;?></title>
    <style type="text/css">
        .container {
          padding: 40px;
        }
        @media (min-width: 768px) {
          .container {
            width: 750px;
          }
        }
        @media (min-width: 992px) {
          .container {
            width: 970px;
          }
        }
        @media (min-width: 1200px) {
          .container {
            width: 1170px;
          }
        }
        .text-center{
          text-align: center;
        }
        table.header tr td{
          font-weight: bold;
        }
        @page { margin: 5px; }
      body{margin:5px;}
      .title, .dept {
        font-size: 22px;
        font-weight: bold;
        width: 100%;
      }
      .title-1{
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      table#konten tr td{
        padding: 0px;
      }
    </style>
</head>
<body>
    <div class="container">
        <table class="header">        
        <tr>
          <td width='200px'>
            <center>
              <img src="<?=base_url('dist/img/logo-jombang.jpg')?>" width="35%" style="margin-bottom: -5px;">
            </center>
          </td>
          <td>
            <div class="title-1">PEMERINTAH KABUPATEN JOMBANG</div>
            <div class="title-1" style="padding-left: 40px;">KECAMATAN MOJOWARNO</div>
            <div class="title" style="padding-left:50px;">DESA CATAKGAYAM</div>
            <span style="font-size: 14px; padding-left: 40px;">Jl. Yos Sudarso Nomor 163 Catakgayam</span>
          </td>
        </tr>
        </table>
        <hr>
        <h4 class="text-center"><u>SURAT PENGANTAR NIKAH</u><br>
        <span>Nomor : <?=$data->no_surat;?></span></h4>
        <p style="text-indent: 60px;">Yang bertandatangan di bawah ini menjelaskan dengan sesungguhnya bahwa :
        </p>
        <table id="konten" style="padding-left: 10px; padding-right: 10px;">
          <tr>
            <td width="240px">Nama</td>
            <td> : <?=$data->nama;?></td>
          </tr>
          <tr>
            <td>Nomor Induk Kependudukan (NIK)</td>
            <td> : <?=$data->nik;?></td>
          </tr>
          <tr>
            <td>Jenis Kelamin</td>
            <td> : <?=$data->jk;?></td>
          </tr>
          <tr>
            <td>Tempat Tanggal Lahir</td>
            <td> : <?=$data->tempat_lahir;?>, <?=date('d-m-Y', strtotime($data->tgl_lahir));?></td>
          </tr>
           <tr>
            <td>Kwarganegaraan</td>
            <td> : <?=$data->kwarganegaraan;?></td>
          </tr>
          <tr>
            <td>Agama</td>
            <td> : <?=$data->agama;?></td>
          </tr>
           <tr>
            <td>Pekerjaan</td>
            <td> : <?=$data->pekerjaan;?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td> : Dsn. <?=$data->dusun;?>, RT. <?=$data->rt;?>, RW. <?=$data->rw;?>  Mojowarno Jombang</td>
          </tr>
          <tr>
            <td>Status Perkawinan</td>
            <td> : <?=$data->status_kawin;?></td>
          </tr>
        
        </table>
        <p style="text-indent: 60px;">
          Adalah benar anak dari perkawinan dari seorang pria :
        </p>

        <?php $ayah = $this->db->where('nik', $data->nik_ayah)->get('penduduk')->row();
         $ibu = $this->db->where('nik', $data->nik_ibu)->get('penduduk')->row(); ?>

        <table id="konten" style="padding-left: 10px; padding-right: 10px;">
          <tr>
            <td width="240px">Nama</td>
            <td> : <?=$ayah->nama;?></td>
          </tr>
          <tr>
            <td>Nomor Induk Kependudukan (NIK)</td>
            <td> : <?=$ayah->nik;?></td>
          </tr>
          <tr>
            <td>Tempat Tanggal Lahir</td>
            <td> : <?=$ayah->tempat_lahir;?>, <?=date('d-m-Y', strtotime($ayah->tgl_lahir));?></td>
          </tr>
           <tr>
            <td>Kwarganegaraan</td>
            <td> : <?=$ayah->kwarganegaraan;?></td>
          </tr>
          <tr>
            <td>Agama</td>
            <td> : <?=$ayah->agama;?></td>
          </tr>
           <tr>
            <td>Pekerjaan</td>
            <td> : <?=$ayah->pekerjaan;?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td> : Dsn. <?=$ayah->dusun;?>, RT. <?=$ayah->rt;?>, RW. <?=$ayah->rw;?>  Mojowarno Jombang</td>
          </tr>
        
        </table>
        <br>
         <table id="konten" style="padding-left: 10px; padding-right: 10px;">
          <tr>
            <td>dengan seorang wanita :</td>
            <td></td>
          </tr>
          <tr>
            <td width="240px">Nama</td>
            <td> : <?=$ibu->nama;?></td>
          </tr>
          <tr>
            <td>Nomor Induk Kependudukan (NIK)</td>
            <td> : <?=$ibu->nik;?></td>
          </tr>
          <tr>
            <td>Tempat Tanggal Lahir</td>
            <td> : <?=$ibu->tempat_lahir;?>, <?=date('d-m-Y', strtotime($ibu->tgl_lahir));?></td>
          </tr>
           <tr>
            <td>Kwarganegaraan</td>
            <td> : <?=$ibu->kwarganegaraan;?></td>
          </tr>
          <tr>
            <td>Agama</td>
            <td> : <?=$ibu->agama;?></td>
          </tr>
           <tr>
            <td>Pekerjaan</td>
            <td> : <?=$ibu->pekerjaan;?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td> : Dsn. <?=$ibu->dusun;?>, RT. <?=$ibu->rt;?>, RW. <?=$ibu->rw;?>  Mojowarno Jombang</td>
          </tr>
        
        </table>

        <table width="100%" style="text-align: center; margin-top: 20px;">
            <tr>
                <td align="center" width="50%"></td>
                <td align="center" width="50%">Catakgayam, <?=date('d-m-Y', strtotime($data->tgl_dibuat))?></td>
            </tr>
            <tr>
              <td align="center" style="padding-bottom: 40px;"></td>
              <td align="center" style="padding-bottom: 40px;">Kepala Desa Catakgayam</td>
            </tr>
            <tr>
              <td></td>
              <td><?=$this->db->get('profil_desa')->row()->kepala_desa;?></td>
            </tr>
      </table>
    </div>
</body>
</html>