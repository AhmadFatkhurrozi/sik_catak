<!DOCTYPE html>
<html>
<head>
    <title><?=$judul;?></title>
    <style type="text/css">
        .container {
          padding: 40px;
        }
        @media (min-width: 768px) {
          .container {
            width: 750px;
          }
        }
        @media (min-width: 992px) {
          .container {
            width: 970px;
          }
        }
        @media (min-width: 1200px) {
          .container {
            width: 1170px;
          }
        }
        .text-center{
          text-align: center;
        }
        table.header tr td{
          font-weight: bold;
        }
        @page { margin: 5px; }
      body{margin:5px;}
      .title, .dept {
        font-size: 22px;
        font-weight: bold;
        width: 100%;
      }
      .title-1{
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      table#konten tr td{
        padding: 10px;
      }
    </style>
</head>
<body>
    <div class="container">
        <table class="header">        
        <tr>
          <td width='200px'>
            <center>
              <img src="<?=base_url('dist/img/logo-jombang.jpg')?>" width="35%" style="margin-bottom: -5px;">
            </center>
          </td>
          <td>
            <div class="title-1">PEMERINTAH KABUPATEN JOMBANG</div>
            <div class="title-1" style="padding-left: 40px;">KECAMATAN MOJOWARNO</div>
            <div class="title" style="padding-left:50px;">DESA CATAKGAYAM</div>
            <span style="font-size: 14px; padding-left: 40px;">Jl. Yos Sudarso Nomor 163 Catakgayam</span>
          </td>
        </tr>
        </table>
        <hr>
        <h3 class="text-center"><u>SURAT KETERANGAN KEMATIAN</u><br>
        <span>Nomor : <?=$data->no_surat;?></span></h3>
        <p style="text-indent: 60px;">Yang bertanda tangan di bawah ini kami Kepala Desa Catakgayam Kecamatan Mojowarno Kabupaten Jombang. </p>
        <p>Menerangkan dengan sebenarnya bahwa orang yang tersebut di bawah ini :
        </p>
        <table id="konten" style="padding-left: 60px; padding-right: 60px;">
          <tr>
            <td>Nama</td>
            <td> :</td>
            <td><?=$data->nama;?></td>
          </tr>
          <tr>
            <td>Umur</td>
            <td> :</td>
            <td><?=date('Y')-date('Y', strtotime($data->tgl_lahir))?> Tahun</td>
          </tr>
          <tr>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td><?=$data->jk;?></td>
          </tr>
          <tr>
            <td>Agama</td>
            <td> :</td>
            <td><?=$data->agama;?></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td> :</td>
            <td>Dsn. <?=$data->dusun;?>, RT. <?=$data->rt;?>, RW. <?=$data->rw;?>  Desa Catakgayam, Kecamatan Mojowarno, Kabupaten Jombang</td>
          </tr>
          <tr>
            <td colspan="3" style="padding-left: 60px;">Adalah benar-benar telah Meninggal Dunia pada</td>
          </tr>
          <tr>
            <td>Hari</td>
            <td> :</td>
            <td><?=$data->hari_meninggal;?></td>
          </tr>
          <tr>
            <td>Tanggal</td>
            <td> :</td>
            <td><?=date('d-m-Y', strtotime($data->tgl_meninggal))?></td>
          </tr>
          <tr>
            <td>Tempat Meninggal</td>
            <td> :</td>
            <td><?=$data->tmpt_meninggal;?></td>
          </tr>
          <tr>
            <td>Sebab Meninggal</td>
            <td> :</td>
            <td><?=$data->sebab_meninggal;?></td>
          </tr>
        </table>
        <p style="text-indent: 60px;">
          Demikian Surat Keterangan ini kami buat dengan sebenarnya untuk dapat dipergunakan sebagaimana mestinya
        </p>
       
        <table width="100%" style="text-align: center; margin-top: 50px;">
            <tr>
                <td align="center" width="50%"></td>
                <td align="center" width="50%">Catakgayam, <?=date('d-m-Y', strtotime($data->tgl_dibuat))?></td>
            </tr>
            <tr>
              <td align="center" style="padding-bottom: 30px;">Yang melapor</td>
              <td align="center" style="padding-bottom: 30px;">Kepala Desa Catakgayam</td>
            </tr>
            <tr>
              <td>(<?=$data->nama_pelapor;?>)</td>
              <td>(<?=$this->db->get('profil_desa')->row()->kepala_desa;?>)</td>
            </tr>
      </table>
    </div>
</body>
</html>