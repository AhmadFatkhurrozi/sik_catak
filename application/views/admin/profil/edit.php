<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <form action="<?=base_url('update-profil')?>" method="post">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <input type="hidden" name="id" value="<?=$profil->id_profil_desa?>">
                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kepala Desa</label>
                        <span class="col-md-7">: <input type="text" name="kepala_desa" value="<?=$profil->kepala_desa;?>"></span>
                    </fieldset>

                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Sekretaris Desa</label>
                        <span class="col-md-7">: <input type="text" name="sekdes" value="<?=$profil->sekdes;?>"></span>
                    </fieldset>

                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kaur Keuangan</label>
                        <span class="col-md-7">: <input type="text" name="kaur_keuangan" value="<?=$profil->kaur_keuangan;?>"></span>
                    </fieldset>

                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kaur Pemerintahan</label>
                        <span class="col-md-7">: <input type="text" name="kaur_pemerintahan" value="<?=$profil->kaur_pemerintahan;?>"></span>
                    </fieldset>
                    
                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kaur Pembangunan</label>
                        <span class="col-md-7">: <input type="text" name="kaur_pembangunan" value="<?=$profil->kaur_pembangunan;?>"></span>
                    </fieldset>
                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kasi Kesra</label>
                        <span class="col-md-7">: <input type="text" name="kasi_kesra" value="<?=$profil->kasi_kesra;?>"></span>
                    </fieldset>

                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kaur Umum</label>
                        <span class="col-md-7">: <input type="text" name="kaur_umum" value="<?=$profil->kaur_umum;?>"></span>
                    </fieldset>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">

                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kasun Catakgayam Utara</label>
                        <span class="col-md-7">: <input type="text" name="kasun_utara" value="<?=$profil->kasun_utara?>"></span>
                    </fieldset>


                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kasun Catakgayam Selatan</label>
                        <span class="col-md-7">: <input type="text" name="kasun_selatan" value="<?=$profil->kasun_selatan;?>"></span>
                    </fieldset>

                    <fieldset class="row form-group">
                        <label class="col-md-5 font-weight-bold">Kasun Tawangsasi</label>
                        <span class="col-md-7">: <input type="text" name="kasun_tawangsari" value="<?=$profil->kasun_tawangsari?>"></span>
                    </fieldset>

                    <button type="submit" class="btn btn-primary pull-right mx-1"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?=base_url('profil-desa');?>" class="btn btn-secondary pull-right mx-1">Kembali</a>
                </div>
            </div>
        </form>
    </div>
    <br>
</div>
