<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kepala Desa</label>
                    <span class="col-md-7">: <?=$profil->kepala_desa;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Sekretaris Desa</label>
                    <span class="col-md-7">: <?=$profil->sekdes;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kaur Keuangan</label>
                    <span class="col-md-7">: <?=$profil->kaur_keuangan;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kaur Pemerintahan</label>
                    <span class="col-md-7">: <?=$profil->kaur_pemerintahan;?></span>
                </fieldset>
                
                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kaur Pembangunan</label>
                    <span class="col-md-7">: <?=$profil->kaur_pembangunan;?></span>
                </fieldset>
                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kasi Kesra</label>
                    <span class="col-md-7">: <?=$profil->kasi_kesra;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kaur Umum</label>
                    <span class="col-md-7">: <?=$profil->kaur_umum;?></span>
                </fieldset>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kasun Catakgayam Utara</label>
                    <span class="col-md-7">: <?=$profil->kasun_utara?></span>
                </fieldset>


                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kasun Catakgayam Selatan</label>
                    <span class="col-md-7">: <?=$profil->kasun_selatan;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kasun Tawangsasi</label>
                    <span class="col-md-7">: <?=$profil->kasun_tawangsari?></span>
                </fieldset>

                <a href="<?=base_url('edit-profil/'.$profil->id_profil_desa)?>" class="btn btn-success btn-sm pull-right"><i class="fa fa-pencil"></i> edit</a>
            </div>
        </div>
    </div>
    <br>
</div>
