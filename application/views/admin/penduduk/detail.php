<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">No. KK</label>
                    <span class="col-md-7">: <?=$penduduk->no_kk;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">NIK</label>
                    <span class="col-md-7">: <?=$penduduk->nik;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Nama</label>
                    <span class="col-md-7">: <?=$penduduk->nama;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Jenis Kelamin</label>
                    <span class="col-md-7">: <?=$penduduk->jk;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Tempat Lahir</label>
                    <span class="col-md-7">: <?=$penduduk->tempat_lahir;?></span>
                </fieldset>
                
                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Tanggal Lahir</label>
                    <span class="col-md-7">: <?=date('d-m-Y', strtotime($penduduk->tgl_lahir))?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Usia</label>
                    <?php $usia = date('Y')-date('Y', strtotime($penduduk->tgl_lahir)); ?>
                    <span class="col-md-7">: <?=$usia;?> Tahun</span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Agama</label>
                    <span class="col-md-7">: <?=$penduduk->agama;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Pekerjaan</label>
                    <span class="col-md-7">: <?=$penduduk->pekerjaan;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Status Kawin</label>
                    <span class="col-md-7">: <?=$penduduk->status_kawin;?></span>
                </fieldset>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Hubungan Keluarga</label>
                    <span class="col-md-7">: <?=$penduduk->status_keluarga?></span>
                </fieldset>


                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Kwarganegaraan</label>
                    <span class="col-md-7">: <?=$penduduk->kwarganegaraan;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Nama Ayah</label>
                    <span class="col-md-7">: <?=$penduduk->nama_ayah?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Nama Ibu</label>
                    <span class="col-md-7">: <?=$penduduk->nama_ibu?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Dusun</label>
                    <span class="col-md-7">: <?=$penduduk->dusun;?></span>
                </fieldset>

                <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Nomor RW</label>
                    <span class="col-md-7">: <?=$penduduk->rw;?></span>
                </fieldset>

                 <fieldset class="row form-group">
                    <label class="col-md-5 font-weight-bold">Nomor RT</label>
                    <span class="col-md-7">: <?=$penduduk->rt;?></span>
                </fieldset>
                <a href="<?=base_url('data-penduduk');?>" class="btn btn-secondary pull-right mx-1">Kembali</a>
            </div>
        </div>
    </div>
    <br>
</div>
