<link rel="stylesheet" type="text/css" href="<?=base_url('plugins/dropzone-master/dist/dropzone.css')?>">
<header class="page-header">

    <div class="container-fluid">

      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>

    </div>

</header>

<br>   

<div class="container-fluid">

    <div class="card card-body">

        <div class="row">
             <div class="col-md-12">
                <!-- Error Form Validation -->
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-warning">
                        <?php print_r(validation_errors()); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-md-6 offset-3">
                <div class="card">
                    <div class="card-header">Import Data</div>
                    <div class="card-body">
                        <div class="form-group">
                            <strong>Panduan Persiapan Import</strong>
                            <ol class="mt-3">
                                <li>Download template <a href="<?php echo base_url(); ?>dist/import_template.xlsx" download="">Disini</a></li>
                                <li>Isi data sesuai template</li>
                                <li>Kamus ID RT, RW, dan Dusun, <a href="<?=base_url('kamus')?>" target="_blank">Cek Disini</a></li>
                                <li>Unggah file</li>
                            </ol>
                            <form action="<?php echo base_url('import-file'); ?>" class="dropzone" id="fileupload">
                            </form>
                        </div>
                        <div class="form-group" id="msg">

                        </div>
                    </div>
                </div>

                <!-- <button type="submit" class="btn btn-primary pull-right mx-1"><i class="fa fa-save"></i> Simpan</button> -->

                <a href="<?=base_url('data-penduduk');?>" class="btn btn-secondary pull-right mx-1">Kembali</a>

            </div>

        </div>


    </div>

<br>

</div>



<script src="<?= base_url(); ?>plugins/dropzone-master/dist/dropzone.js"></script>

<script>
    Dropzone.options.fileupload = {
        acceptedFiles: '.xlsx',
        maxFilesize: 1, // MB
        maxFiles: 1,
        addRemoveLinks: true,
        init: function() {
            this.on("sending", function() {
                $('#msg').html('<h1 class="display-3 text-center">...</h1>');
            });
            this.on("complete", function(file) {
                $.get('<?php echo base_url('do-import-penduduk'); ?>', {}, function(e) {
                    $('#msg').css({
                        'height': '300px',
                        'overflow-y': 'auto'
                    });

                    $('#msg').html(e);
                });
            });
        }
    };
</script>