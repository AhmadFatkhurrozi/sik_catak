<header class="page-header">

    <div class="container-fluid">

      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>

    </div>

</header>

<br>   

<div class="container-fluid">

    <div class="card card-body">

        <form action="<?=base_url('update-penduduk')?>" method="post">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12">

                    <input type="hidden" name="id" value="<?=$penduduk->id;?>">



                     <fieldset class="form-group">

                        <label>No.KK</label>

                        <input type="text" autocomplete="off" class="form-control" name="no_kk" value="<?=$penduduk->no_kk;?>" id="kk">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>NIK</label>

                        <input type="text" autocomplete="off" class="form-control" name="nik" value="<?=$penduduk->nik;?>" readonly="">

                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nama</label>

                        <input type="text" autocomplete="off" name="nama" value="<?=$penduduk->nama;?>" class="form-control">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Jenis Kelamin</label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="jk" <?=($penduduk->jk == "Laki-laki") ? "checked": "";?> value="Laki-laki"> Laki-laki

                        </label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="jk" <?=($penduduk->jk == "Perempuan") ? "checked": "";?> value="Perempuan"> Perempuan

                        </label>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Tempat Lahir</label>

                        <input type="text" autocomplete="off" name="tempat_lahir" class="form-control" value="<?=$penduduk->tempat_lahir;?>">

                    </fieldset>

                    

                    <fieldset class="form-group">

                        <label>Tanggal Lahir</label>

                        <input type="date" autocomplete="off" name="tgl_lahir" class="form-control" value="<?=$penduduk->tgl_lahir;?>">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Agama</label>

                        <select class="form-control" name="agama">

                            <option value="" disabled=""> .:: Pilih Agama ::.</option>

                            <option <?=($penduduk->agama == "Islam") ? "selected" : "";?> value="Islam">Islam</option>

                            <option <?=($penduduk->agama == "Kristen Protestan") ? "selected" : "";?> value="Kristen Protestan">Kristen Protestan</option>

                            <option <?=($penduduk->agama == "Kristen Katolik") ? "selected" : "";?> value="Kristen Katolik">Kristen Katolik</option>

                            <option <?=($penduduk->agama == "Hindu") ? "selected" : "";?> value="Hindu">Hindu</option>

                            <option <?=($penduduk->agama == "Buddha") ? "selected" : "";?> value="Buddha">Buddha</option>

                            <option <?=($penduduk->agama == "Khonghucu") ? "selected" : "";?> value="Khonghucu">Khonghucu</option>

                        </select>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Pekerjaan</label>

                        <select class="form-control" name="pekerjaan" required="">

                          <option disabled="" value="">---Pilih Pekerjaan---</option>

                          <option <?=($penduduk->pekerjaan == "Pegawai Negeri") ? "selected" : "";?> value="Pegawai Negeri">Pegawai Negeri</option>

                          <option <?=($penduduk->pekerjaan == "ABRI") ? "selected" : "";?> value="ABRI">ABRI</option>

                          <option <?=($penduduk->pekerjaan == "Anggota DPR") ? "selected" : "";?> value="Anggota DPR">Anggota DPR</option>

                          <option <?=($penduduk->pekerjaan == "Pegawai Swasta") ? "selected" : "";?> value="Pegawai Swasta">Pegawai Swasta</option>

                          <option <?=($penduduk->pekerjaan == "Pedagang") ? "selected" : "";?> value="Pedagang">Pedagang</option>

                          <option <?=($penduduk->pekerjaan == "Petani") ? "selected" : "";?> value="Petani">Petani</option>

                          <option <?=($penduduk->pekerjaan == "Pengusaha") ? "selected" : "";?> value="Pengusaha">Pengusaha</option>

                          <option <?=($penduduk->pekerjaan == "Lain-lain") ? "selected" : "";?> value="Lain-lain">Lain-lain</option>

                        </select>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Status Kawin</label>

                        <select class="form-control" name="status_kawin">

                            <option value="" disabled=""> .:: Pilih Status Kawin ::.</option>

                            <option <?=($penduduk->status_kawin == "Belum kawin") ? "selected" : "";?>value="Belum kawin">Belum kawin</option>

                            <option <?=($penduduk->status_kawin == "Kawin") ? "selected" : "";?>value="Kawin">Kawin</option>

                            <option <?=($penduduk->status_kawin == "Cerai hidup") ? "selected" : "";?>value="Cerai hidup">Cerai hidup</option>

                            <option <?=($penduduk->status_kawin == "Cerai mati") ? "selected" : "";?>value="Cerai mati">Cerai mati</option>

                        </select>

                    </fieldset>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">

                    <fieldset class="form-group">

                        <label>Hubungan Keluarga</label>

                        <select class="form-control" name="status_keluarga">

                            <option selected="" value="" disabled=""> .:: Pilih Status Keluarga ::.</option>

                            <option <?=($penduduk->status_keluarga == "Kepala Keluarga") ? "selected" : "";?> value="Kepala Keluarga">Kepala Keluarga</option>

                            <option <?=($penduduk->status_keluarga == "Anak") ? "selected" : "";?> value="Anak">Anak</option>

                            <option <?=($penduduk->status_keluarga == "Istri") ? "selected" : "";?> value="Istri">Istri</option>

                        </select>

                    </fieldset>





                    <fieldset class="form-group">

                        <label>Kwarganegaraan</label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="warga" value="WNI" <?=($penduduk->kwarganegaraan == "WNI") ? "checked" : "";?>> WNI

                        </label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="warga" value="WNA" <?=($penduduk->kwarganegaraan == "WNA") ? "checked" : "";?>> WNA

                        </label>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nama Ayah</label>

                        <input type="text" autocomplete="off" name="nama_ayah" class="form-control" value="<?=$penduduk->nama_ayah?>">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nama Ibu</label>

                        <input type="text" autocomplete="off" name="nama_ibu" class="form-control" value="<?=$penduduk->nama_ibu?>">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Dusun</label>

                        <select class="form-control" name="dusun_id" id="dusun_id" onchange="getRw()">

                            <?php foreach ($dusun->result() as $key) { ?>

                                <option value="<?=$key->id_dusun?>" <?=($penduduk->dusun == $key->nama_dusun) ? "selected" : ""; ?>><?=$key->nama_dusun;?></option>

                            <?php } ?>

                        </select>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nomor RW</label>

                        <select class="form-control" name="rw_id" id="rw_id" onchange="getRt()">

                        </select>

                    </fieldset>



                     <fieldset class="form-group">

                        <label>Nomor RT</label>

                        <select class="form-control" name="rt_id" id="rt_id" onchange="getRt()">

                        </select>

                    </fieldset>



                    <button type="submit" class="btn btn-primary pull-right mx-1"><i class="fa fa-save"></i> Simpan</button>

                    <a href="<?=base_url('data-penduduk');?>" class="btn btn-secondary pull-right mx-1">Kembali</a>

                </div>

            </div>

        </form>     

    </div>

    <br>

</div>



<script type="text/javascript">

    $(document).ready(function(){

      getRw();

      getRt();

    });



    function getRw(){

        var dusun_id = $('#dusun_id').val();

        $.ajax({

            type: 'POST',

            url: '<?php echo site_url('get-rw')?>',

            async : true,

            dataType : 'json',

            data: {id_dusun:dusun_id},

            success: function(response) { 

                if(response.status == 'success'){

                  var html = '<option value="<?=$rw?>" selected><?=$penduduk->rw?></option>';

                  $.each(response.data,function(k,v){

                    html += '<option value="'+v.id_rw+'">'+v.nomor_rw+'</option>';

                  });

                  $('#rw_id').html(html);

                  // $('#rw_id').trigger("chosen:updated");

                } else {

                  return "";

                }

            }

        });

    } 



    function getRt(){

        var rw_id = $('#rw_id').val();

        $.ajax({

            type: 'POST',

            url: '<?php echo site_url('get-rt')?>',

            async : true,

            dataType : 'json',

            data: {id_rw:rw_id},

            success: function(response) { 

                if(response.status == 'success'){

                  var html = '<option value="<?=$rt?>" selected><?=$penduduk->rt?></option>';

                  $.each(response.data,function(k,v){

                    html += '<option value="'+v.id_rt+'">'+v.nomor_rt+'</option>';

                  });

                  $('#rt_id').html(html);

                  // $('#rw_id').trigger("chosen:updated");

                } else {

                  return "";

                }

            }

        });

    } 

</script>