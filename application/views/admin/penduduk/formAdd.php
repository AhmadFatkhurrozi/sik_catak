<header class="page-header">

    <div class="container-fluid">

      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>

    </div>

</header>

<br>   

<div class="container-fluid">

    <div class="card card-body">

        <form action="<?=base_url('add-penduduk')?>" method="post">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12">

                    <fieldset class="form-group">

                        <label>No.KK</label>

                        <input type="text" autocomplete="off" class="form-control" name="no_kk" placeholder="masukkan nomor KK" id="kk">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>nik</label>

                        <input type="text" autocomplete="off" class="form-control" name="nik" placeholder="masukkan nik" id="nik">

                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nama</label>

                        <input type="text" autocomplete="off" name="nama" class="form-control" placeholder="masukkan nama lengkap">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Jenis Kelamin</label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="jk" value="Laki-laki"> Laki-laki

                        </label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="jk" value="Perempuan"> Perempuan

                        </label>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Tempat Lahir</label>

                        <input type="text" autocomplete="off" name="tempat_lahir" class="form-control" placeholder="masukkan tempat lahir">

                    </fieldset>

                    

                    <fieldset class="form-group">

                        <label>Tanggal Lahir</label>

                        <input type="date" autocomplete="off" name="tgl_lahir" class="form-control" placeholder="masukkan tanggal lahir">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Agama</label>

                        <select class="form-control" name="agama">

                            <option selected="" value="" disabled=""> .:: Pilih Agama ::.</option>

                            <option value="Islam">Islam</option>

                            <option value="Kristen Protestan">Kristen Protestan</option>

                            <option value="Kristen Katolik">Kristen Katolik</option>

                            <option value="Hindu">Hindu</option>

                            <option value="Buddha">Buddha</option>

                            <option value="Khonghucu">Khonghucu</option>

                        </select>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Pekerjaan</label>

                        <select class="form-control" name="pekerjaan" required="">

                          <option value="">---Pilih Pekerjaan---</option>

                          <option value="Pegawai Negeri">Pegawai Negeri</option>

                          <option value="ABRI">ABRI</option>

                          <option value="Anggota DPR">Anggota DPR</option>

                          <option value="Pegawai Swasta">Pegawai Swasta</option>

                          <option value="Pedagang">Pedagang</option>

                          <option value="Petani">Petani</option>

                          <option value="Pengusaha">Pengusaha</option>

                          <option value="Lain-lain">Lain-lain</option>

                        </select>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Status Kawin</label>

                        <select class="form-control" name="status_kawin">

                            <option selected="" value="" disabled=""> .:: Pilih Status Kawin ::.</option>

                            <option value="Belum kawin">Belum kawin</option>

                            <option value="Kawin">Kawin</option>

                            <option value="Cerai hidup">Cerai hidup</option>

                            <option value="Cerai mati">Cerai mati</option>

                        </select>

                    </fieldset>



                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">

                    <fieldset class="form-group">

                        <label>Hubungan Keluarga</label>

                        <select class="form-control" name="status_keluarga">

                            <option selected="" value="" disabled=""> .:: Pilih Status Keluarga ::.</option>

                            <option value="Kepala Keluarga">Kepala Keluarga</option>

                            <option value="Anak">Anak</option>

                            <option value="Istri">Istri</option>

                        </select>

                    </fieldset>





                    <fieldset class="form-group">

                        <label>Kwarganegaraan</label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="warga" value="WNI"> WNI

                        </label>

                        <label class="radio-inline mx-3">

                            <input type="radio" name="warga" value="WNA"> WNA

                        </label>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nama Ayah</label>

                        <input type="text" autocomplete="off" name="nama_ayah" class="form-control" placeholder="masukkan nama ayah">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nama Ibu</label>

                        <input type="text" autocomplete="off" name="nama_ibu" class="form-control" placeholder="masukkan nama ibu">

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Dusun</label>

                        <select class="form-control" name="dusun_id" onchange="getRw(this)">

                            <option selected="" value="" disabled=""> .:: Pilih Dusun ::.</option>

                            <?php foreach ($dusun->result() as $key) { ?>

                                <option value="<?=$key->id_dusun?>"><?=$key->nama_dusun;?></option>

                            <?php } ?>

                        </select>

                    </fieldset>



                    <fieldset class="form-group">

                        <label>Nomor RW</label>

                        <select class="form-control" name="rw_id" id="rw_id" onchange="getRt(this)">

                        </select>

                    </fieldset>



                     <fieldset class="form-group">

                        <label>Nomor RT</label>

                        <select class="form-control" name="rt_id" id="rt_id" onchange="getRt(this)">

                        </select>

                    </fieldset>



                    <button type="submit" class="btn btn-primary pull-right mx-1"><i class="fa fa-save"></i> Simpan</button>

                    <a href="<?=base_url('data-penduduk');?>" class="btn btn-secondary pull-right mx-1">Kembali</a>

                </div>

            </div>

        </form> 

    </div>

<br>

</div>



<script type="text/javascript">

    $('#nik').keypress(function(event){

       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){

           event.preventDefault();

       }

   });



    function getRw(ini){

        $.ajax({

            type: 'POST',

            url: '<?php echo site_url('get-rw')?>',

            async : true,

            dataType : 'json',

            data: {id_dusun:ini.value},

            success: function(response) { 

                if(response.status == 'success'){

                  var html = '<option value="">.:: Nomor RW ::.</option>';

                  $.each(response.data,function(k,v){

                    html += '<option value="'+v.id_rw+'">'+v.nomor_rw+'</option>';

                  });

                  $('#rw_id').html(html);

                  // $('#rw_id').trigger("chosen:updated");

                } else {

                  return "";

                }

            }

        });

    } 



    function getRt(ini){

        $.ajax({

            type: 'POST',

            url: '<?php echo site_url('get-rt')?>',

            async : true,

            dataType : 'json',

            data: {id_rw:ini.value},

            success: function(response) { 

                if(response.status == 'success'){

                  var html = '<option value="">.:: Nomor RT ::.</option>';

                  $.each(response.data,function(k,v){

                    html += '<option value="'+v.id_rt+'">'+v.nomor_rt+'</option>';

                  });

                  $('#rt_id').html(html);

                  // $('#rw_id').trigger("chosen:updated");

                } else {

                  return "";

                }

            }

        });

    } 

</script>