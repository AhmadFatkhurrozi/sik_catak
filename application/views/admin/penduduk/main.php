<header class="page-header">
    <div class="container-fluid">
        <?php 
            if($usia == 17){
                $judulUsia = ": Usia Diatas 17 tahun";
            // }elseif($usia == "dibawah-17"){ 
            //     $judulUsia = ": Usia Dibawah 17 tahun";
            }else{ 
                $judulUsia = "";
            } 
        ?>
        
        <h2 class="no-margin-bottom"><?php echo $judul.' '.$judulUsia; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <a href="<?=base_url('form-penduduk')?>" class="btn btn-primary btn-sm">Tambah Data</a>
    <!-- <a href="javascript:void(0)" class="btn btn-danger btn-sm float-right mr-2"><i class="fa fa-file-excel-o"></i> Export</a> -->
    <a href="<?=base_url('import-penduduk')?>" class="btn btn-success btn-sm"><i class="fa fa-upload"></i> Import Data</a>
    <select class="form-inline form-control-sm float-right mr-2" name="usia" id="usia" onchange="filterPenduduk()">
        <option selected="" value="">-- Usia --</option>
        <option value="">Semua</option>
        <!-- <option value="dibawah-17">Kurang dari 17 tahun</option> -->
        <option value="17">17 tahun keatas+</option>
    </select>
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Usia</th>
                <th>RT</th>
                <th>RW</th>
                <th>Dusun</th>
                <th>Status</th>
                <th style="text-align: center;">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($data->result() as $a) { ?>
            <?php $vUsia = date('Y')-date('Y', strtotime($a->tgl_lahir)); ?>
            <?php if(!empty($usia)){?>
                <?php if($vUsia >= $usia){ ?>
                    <tr>
                        <td align="center"><?=$no++; ?></td>
                        <td><?=$a->nik;?></td>
                        <td><?=$a->nama;?></td>
                        <td><?=$vUsia;?> thn</td>
                        <td><?=$a->rt;?></td>
                        <td><?=$a->rw;?></td>
                        <td><?=$a->dusun;?></td>
                        <td><?=$a->status;?></td>
                        <td align="center">
                            <a href="<?=base_url('edit-penduduk/'.$a->id)?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> edit</a>
                            <a href="<?=base_url('detail-penduduk/'.$a->id)?>" class="btn btn-info btn-sm"><i class="fa fa-info"></i> detail</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td align="center"><?=$no++; ?></td>
                    <td><?=$a->nik;?></td>
                    <td><?=$a->nama;?></td>
                    <td><?=$vUsia;?> thn</td>
                    <td><?=$a->rt;?></td>
                    <td><?=$a->rw;?></td>
                    <td><?=$a->dusun;?></td>
                    <td><?=$a->status;?></td>
                    <td align="center">
                        <a href="<?=base_url('edit-penduduk/'.$a->id)?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> edit</a>
                        <a href="<?=base_url('detail-penduduk/'.$a->id)?>" class="btn btn-info btn-sm"><i class="fa fa-info"></i> detail</a>
                    </td>
                </tr>
            <?php }} ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    });

    function filterPenduduk() {
        var usia  = $('#usia').val();
        window.open("<?=base_url('data-penduduk')?>?usia="+usia, "_self");
    }
</script>