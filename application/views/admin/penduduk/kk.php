<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <a href="<?=base_url('form-penduduk')?>" class="btn btn-primary btn-sm">Tambah Data</a>
  
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>No. KK</th>
                <th>Nama Kepala Keluarga</th>
                <th>RT</th>
                <th>RW</th>
                <th>Dusun</th>
                <th>Status</th>
                <th style="text-align: center;">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($data->result() as $a) { ?>
            <tr>
                <td align="center"><?=$no++; ?></td>
                <td><?=$a->no_kk;?></td>
                <td><?=$a->nama;?></td>
                <td><?=$a->rt;?></td>
                <td><?=$a->rw;?></td>
                <td><?=$a->dusun;?></td>
                <td><?=$a->status;?></td>
                <td align="center">
                    <a href="<?=base_url('detail-keluarga/'.$a->no_kk)?>" class="btn btn-info btn-sm"><i class="fa fa-info"></i> detail</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    });

</script>