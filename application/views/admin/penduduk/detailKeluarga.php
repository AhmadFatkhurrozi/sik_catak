<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <a href="<?=base_url('data-kk')?>" class="btn btn-secondary btn-sm">Kembali</a>
  
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Status Keluarga</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($penduduk->result() as $a) { ?>
            <tr>
                <td align="center"><?=$no++; ?></td>
                <td><?=$a->nik;?></td>
                <td><a href="<?=base_url('detail-penduduk/'.$a->id)?>"><?=$a->nama;?></a></td>
                <td><?=$a->tempat_lahir;?></td>
                <td><?=date('d-m-Y', strtotime($a->tgl_lahir))?></td>
                <td><?=$a->status_keluarga;?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    });

</script>