<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>NIK</th>
                <th>Nama</th>
                <th>RT</th>
                <th>RW</th>
                <th>Dusun</th>
                <th>Tanggal Dibuat</th>
                <th style="text-align: center;">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($data->result() as $a) { ?>
            <tr>
                <td align="center"><?=$no++; ?></td>
                <td><?=$a->nik;?></td>
                <td><?=$a->nama;?></td>
                <td><?=$a->rt;?></td>
                <td><?=$a->rw;?></td>
                <td><?=$a->dusun;?></td>
                <td><?=date('d-m-Y', strtotime($a->tgl_dibuat));?></td>
                <td align="center">
                     <a href="<?=base_url('cetak-surat-nikah/'.$a->nik);?>" target="_blank" class="btn btn-danger btn-sm pull-right"><i class="fa fa-print"></i> Cetak</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>