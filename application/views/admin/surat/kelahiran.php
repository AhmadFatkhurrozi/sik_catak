<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>Nama Anak</th>
                <th>Tempat, Tanggal Lahir</th>
                <th>Nama Ayah</th>
                <th>Nama Ibu</th>
                <th>KMS</th>
                <th>Alamat</th>
                <th style="text-align: center;">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($data->result() as $a) { ?>
            <tr>
                <td align="center"><?=$no++; ?></td>
                <td><?=$a->nama_anak;?></td>
                <td><?=$a->tmpt_lahir.' '.date('d-m-Y', strtotime($a->tgl_lahir));?></td>
                <td><?=$a->nama_ayah;?></td>
                <td><?=$a->nama_ibu;?></td>
                <td><img height="40px" src="<?=base_url('upload/'.$a->foto_kms);?>"></td>
                <td><?=$a->alamat;?></td>
                <td align="center">
                     <a href="<?=base_url('cetak-surat-kelahiran/'.$a->id_kelahiran);?>" target="_blank" class="btn btn-danger btn-sm pull-right"><i class="fa fa-print"></i> Cetak</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>