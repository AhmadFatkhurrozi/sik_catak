<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid mt-4">
   <div class="row">
     <div class="col-lg-3 col-md-3 col-sm-12 col-12">
        <fieldset class="form-group">
           <select class="form-control" id="dusun_id" onchange="getRw(this)">
             <option disabled="" selected="">.:: Pilih Dusun ::.</option>
             <?php foreach ($dusun->result() as $key) { ?>
                <option value="<?=$key->id_dusun?>"><?=$key->nama_dusun;?></option>
             <?php } ?>
           </select>
         </fieldset>
      </div>

     <div class="col-lg-2 col-md-2 col-sm-12 col-12">
         <fieldset class="form-group">
            <select class="form-control" name="rw_id" id="rw_id" onchange="getRt(this)">
            </select>
         </fieldset>
     </div>

     <div class="col-lg-2 col-md-2 col-sm-12 col-12">
         <fieldset class="form-group">
            <select class="form-control" name="rt_id" id="rt_id" onchange="getRt(this)">
            </select>
         </fieldset>
     </div>
     <div class="col-lg-2 col-md-2 col-sm-12 col-12">

         <select class="form-control" name="usia" id="usia">
            <option selected="" value="">-- Usia --</option>
            <option value="">Semua</option>
            <!-- <option value="dibawah-17">Kurang dari 17 tahun</option> -->
            <option value="17">17 tahun keatas</option>
        </select>
      </div>

     <div class="col-lg-3 col-md-3 col-sm-12 col-12 ">
         <a href="javascript:void(0)" class="btn btn-success text-light mr-2" onclick="cariPenduduk()"><i class="fa fa-search"></i> Cari</a>
         <a href="javascript:void(0)" class="btn btn-danger mr-2 btnExport" style="display: none;" onclick="toExcel('my-table-id', 'DATA PENDUDUK')"><i class="fa fa-file-excel-o"></i> Export</a>
     </div>
   </div>
   <hr>
   <div class="row">
      <div class="col-lg-12 col-md-12">
         <table class="table table-bordered table-hover table-sm" id="my-table-id">
           <thead class="bg-primary text-light">
               <tr>
                   <th width="35px">No</th>
                   <th style="display: none;">No. KK</th>
                   <th>NIK</th>
                   <th>Nama</th>
                   <th>RT</th>
                   <th>RW</th>
                   <th>Dusun</th>
                   <th>Usia</th>
                   <th style="display: none;">Jenis Kelamin</th>
                   <th style="display: none;">Tempat Lahir</th>
                   <th style="display: none;">Tanggal Lahir</th>
                   <th style="display: none;">Agama</th>
                   <th style="display: none;">Pekerjaan</th>
                   <th style="display: none;">Status Kawin</th>
                   <th style="display: none;">Status Keluarga</th>
                   <th style="display: none;">Kwarganegaraan</th>
                   <th style="display: none;">Nama Ayah</th>
                   <th style="display: none;">Nama Ibu</th>
               </tr>
           </thead>
           <tbody id="hasil">
                <tr>
                <td style="padding: 5px;" colspan="7" align="center"> .:: pilih data untuk ditampilkan ::. </td>
                </tr>
           </tbody>
         </table>
      </div>
   </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // $('#datatable').DataTable();
    });

     function getRw(ini){
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('get-rw')?>',
            async : true,
            dataType : 'json',
            data: {id_dusun:ini.value},
            success: function(response) { 
                if(response.status == 'success'){
                  var html = '<option value="">.:: Nomor RW ::.</option>';
                  $.each(response.data,function(k,v){
                    html += '<option value="'+v.id_rw+'">'+v.nomor_rw+'</option>';
                  });
                  $('#rw_id').html(html);
                  // $('#rw_id').trigger("chosen:updated");
                } else {
                  return "";
                }
            }
        });
    } 

    function getRt(ini){
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('get-rt')?>',
            async : true,
            dataType : 'json',
            data: {id_rw:ini.value},
            success: function(response) { 
                if(response.status == 'success'){
                  var html = '<option value="">.:: Nomor RT ::.</option>';
                  $.each(response.data,function(k,v){
                    html += '<option value="'+v.id_rt+'">'+v.nomor_rt+'</option>';
                  });
                  $('#rt_id').html(html);
                  // $('#rw_id').trigger("chosen:updated");
                } else {
                  return "";
                }
            }
        });
    } 

    function cariPenduduk() {
      var dusun_id = $('#dusun_id').val();
      var rw_id = $('#rw_id').val();
      var rt_id = $('#rt_id').val();
      var usia = $('#usia').val();
      $('#hasil').html("");
       $.ajax({
            type: 'POST',
            url: '<?php echo site_url('cari-penduduk')?>',
            async : true,
            dataType : 'json',
            data: {dusun_id:dusun_id, rw_id:rw_id, rt_id:rt_id, usia:usia},
            success: function(response) { 
                var tag = '';
                if(response.status == 'success'){
                    $('.btnExport').show();
                    var no = 0;
                    $.each(response.data, function(k,h){
                        var nomer = 1 + no++;
                        var dt = new Date();
                        var tgl = h.tgl_lahir;
                        var pecahTgl = tgl.split(' ');
                        var pecahTgl2 = pecahTgl[0].split('-');
                        var vUsia = dt.getFullYear()-pecahTgl2[0];

                        if (response.usia != '') {
                            if (vUsia >= response.usia) {
                                tag += '<tr>';
                                tag += '<td style="padding: 5px;" align="center" valign="top">'+nomer+'</td>';
                                tag += '<td style="padding: 5px; display: none;" align="center" valign="top">'+String(h.no_kk)+'</td>';
                                tag += '<td style="padding: 5px;" align="center" valign="top">'+String(h.nik)+'</td>';
                                tag += '<td style="padding: 5px;" align="center" valign="top">'+h.nama+'</td>';
                                tag += '<td style="padding: 5px;" align="center" valign="top">'+h.rt+'</td>';
                                tag += '<td style="padding: 5px;" align="center" valign="top">'+h.rw+'</td>';
                                tag += '<td style="padding: 5px;" align="center" valign="top">'+h.dusun+'</td>';


                                tag += '<td style="padding: 5px;" align="center" valign="top">'+vUsia+' tahun</td>';
                                tag += '<td style="display: none;">'+h.jk+'</td>';
                                tag += '<td style="display: none;">'+h.tempat_lahir+'</td>';
                                tag += '<td style="display: none;">'+pecahTgl2[2]+'/'+pecahTgl2[1]+'/'+pecahTgl2[0]+'</td>';
                                tag += '<td style="display: none;">'+h.agama+'</td>';
                                tag += '<td style="display: none;">'+h.pekerjaan+'</td>';
                                tag += '<td style="display: none;">'+h.status_kawin+'</td>';
                                tag += '<td style="display: none;">'+h.status_keluarga+'</td>';
                                tag += '<td style="display: none;">'+h.kwarganegaraan+'</td>';
                                tag += '<td style="display: none;">'+h.nama_ayah+'</td>';
                                tag += '<td style="display: none;">'+h.nama_ibu+'</td>';
                                tag += '</tr>';
                            }
                        }else{
                            tag += '<tr>';
                            tag += '<td style="padding: 5px;" align="center" valign="top">'+nomer+'</td>';
                            tag += '<td style="padding: 5px; display: none;" align="center" valign="top">'+String(h.no_kk)+'</td>';
                            tag += '<td style="padding: 5px;" align="center" valign="top">'+String(h.nik)+'</td>';
                            tag += '<td style="padding: 5px;" align="center" valign="top">'+h.nama+'</td>';
                            tag += '<td style="padding: 5px;" align="center" valign="top">'+h.rt+'</td>';
                            tag += '<td style="padding: 5px;" align="center" valign="top">'+h.rw+'</td>';
                            tag += '<td style="padding: 5px;" align="center" valign="top">'+h.dusun+'</td>';


                            tag += '<td style="padding: 5px;" align="center" valign="top">'+vUsia+' tahun</td>';
                            tag += '<td style="display: none;">'+h.jk+'</td>';
                            tag += '<td style="display: none;">'+h.tempat_lahir+'</td>';
                            tag += '<td style="display: none;">'+pecahTgl2[2]+'/'+pecahTgl2[1]+'/'+pecahTgl2[0]+'</td>';
                            tag += '<td style="display: none;">'+h.agama+'</td>';
                            tag += '<td style="display: none;">'+h.pekerjaan+'</td>';
                            tag += '<td style="display: none;">'+h.status_kawin+'</td>';
                            tag += '<td style="display: none;">'+h.status_keluarga+'</td>';
                            tag += '<td style="display: none;">'+h.kwarganegaraan+'</td>';
                            tag += '<td style="display: none;">'+h.nama_ayah+'</td>';
                            tag += '<td style="display: none;">'+h.nama_ibu+'</td>';
                            tag += '</tr>';
                        }
                    });
                    no = no + response.data.length;

                }else{
                    $('.btnExport').hide();

                    tag += '<tr>';
                    tag += '<td style="padding: 5px;" colspan="7" align="center"> .:: data tidak ditemukan ::. </td>';
                    tag += '</tr>';
                }
                $('#hasil').append(tag);
            }
        });
    }

    var toExcel = (function() {
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        window.location.href = uri + base64(format(template, ctx))
      }
    })()
</script>