<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <form action="<?=base_url('simpan-kematian')?>" method="post">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="text-center">Pelapor</label>
                    <hr>

                    <fieldset class="form-group">
                        <label>Nama Pelapor</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_pelapor" id="nama_pelapor">
                    </fieldset>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12">
                    <label class="text-center">Data Orang yang Meninggal</label>
                    <hr>
                    <fieldset class="form-group">
                        <label>nik</label>
                        <input type="text" autocomplete="off" onchange="getDataPenduduk()" class="form-control" name="nik" placeholder="masukkan nik" id="nik">
                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Nama</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama" readonly="" id="nama">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" readonly="" id="alamat"></textarea>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Hari</label>
                        <select name="hari" class="form-control">
                          <option disabled="" selected="">.:: pilih hari ::.</option>
                          <option value="Senin">Senin</option>
                          <option value="Selasa">Selasa</option>
                          <option value="Rabu">Rabu</option>
                          <option value="Kamis">Kamis</option>
                          <option value="Jumat">Jumat</option>
                          <option value="Sabtu">Sabtu</option>
                          <option value="Minggu">Minggu</option>
                        </select>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tanggal</label>
                        <input type="date" autocomplete="off" class="form-control" name="tanggal">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Tempat Meninggal</label>
                        <input type="text" autocomplete="off" class="form-control" name="tempat">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Sebab Meninggal</label>
                        <input type="text" autocomplete="off" class="form-control" name="sebab">
                    </fieldset>

                    <button type="submit" class="btn btn-primary pull-right mx-1"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?=base_url('data-penduduk');?>" class="btn btn-secondary pull-right mx-1">Kembali</a>
                </div>
            </div>
        </form> 
    </div>
<br>
</div>

<script type="text/javascript">
  function getDataPenduduk() {
    var nik = $('#nik').val();
    $.ajax({
            type: 'POST',
            url: '<?php echo site_url('getDataPenduduk')?>',
            async : true,
            dataType : 'json',
            data: {nik:nik},
            success: function(response) { 
                if(response.status == 'success'){
                  $('#nama').val(response.data.nama);
                  var alm = "Dsn. "+response.data.dusun+", RT. "+response.data.rt+", RW. "+response.data.rw+" Desa Catakgayam, Kecamatan   Mojowarno, Kabupaten Jombang";
                  $('#alamat').val(alm);
                  $('#notifications').html('');
                } else {
                  $('#notifications').html('nik tidak ditemukan');
                }
            }
        });
  }
</script>