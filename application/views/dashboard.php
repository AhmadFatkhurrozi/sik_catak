<style type="text/css">
    .my-custom-scrollbar {
position: relative;
height: 80px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
table tr th, table tr td{
    padding: 0px !important;
}
</style>
<section class="dashboard-counts no-padding-bottom">
	<div class="">
    	<div class="alert alert-info text-dark">
    		<h1 class="display-5">Selamat Datang - <?php echo $this->session->userdata('get_nama'); ?></h1>
    	</div>

    	<!-- Content Row -->
        <div class="row">

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                	Pengguna    
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->count_all('users'); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-users fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Penduduk</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->count_all('penduduk'); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-users fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat Kelahiran    
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->count_all('surat_kelahiran'); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-list-alt fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Surat Kematian    
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->count_all('surat_kematian'); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-ambulance fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    SKCK    
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->count_all('skck'); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-file fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <div class="col-xl-4 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    SKTM    
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->db->count_all('sktm'); ?></div>
                            </div>
                            <div class="col-auto">
                                <i class="fa fa-envelope fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <!--  <div class="col-xl-4 col-md-6">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Pengajuan Surat</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">0</div>
                              <div class="table-wrapper-scroll-y my-custom-scrollbar">

                                  <table class="table table-bordered table-striped mb-0">
                                    <thead>
                                      <tr>
                                        <th scope="col">Jenis Surat</th>
                                        <th scope="col">Total</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Surat Kelahiran</td>
                                            <td><?=$this->db->count_all('surat_kelahiran'); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Surat Kematian</td>
                                            <td><?=$this->db->count_all('surat_kematian'); ?></td>
                                        </tr>
                                        <tr>
                                            <td>SKCK</td>
                                            <td><?=$this->db->count_all('skck'); ?></td>
                                        </tr>
                                        <tr>
                                            <td>SKTM</td>
                                            <td><?=$this->db->count_all('sktm'); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Surat Pindah</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Surat Nikah</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                  </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->

        </div>

        <div class="row" style="padding: 10px 0px;">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div id="grafikPekerjaan"></div>
            </div>
        </div>
        <div class="row" style="padding: 10px 0px;">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <div id="container"></div>
            </div>
        
            <div class="col-lg-9 col-md-9 col-sm-12">
                <div id="grafikUsia"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    Highcharts.setOptions({
        colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
            return {
                radialGradient: {
                    cx: 0.5,
                    cy: 0.3,
                    r: 0.7
                },
                stops: [
                    [0, color],
                    [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
                ]
            };
        })
    });

    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Statistik Penduduk Berdasarkan Jenis Kelamin'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f} jiwa</b>'
        },
        accessibility: {
            point: {
                valueSuffix: ' jiwa'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'sebanyak',
            colorByPoint: true,
            data: <?=$jk;?>
        }]
    });        

    Highcharts.chart('grafikPekerjaan', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Statistik Penduduk Berdasarkan Pekerjaan'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f} jiwa</b>'
        },
        accessibility: {
            point: {
                valueSuffix: 'jiwa'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.0f} jiwa '
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: <?=$pekerjaan;?>
        }]
    });  

    Highcharts.chart('grafikUsia', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Statistik Penduduk Berdasarkan Usia'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f} jiwa</b>'
        },
        accessibility: {
            point: {
                valueSuffix: 'jiwa'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y:.0f} jiwa '
                }
            }
        },
        series: [{
            name: 'Total',
            colorByPoint: true,
            data: <?=$usia;?>
        }]
    });   

     
</script>s
