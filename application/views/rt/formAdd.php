<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form action="<?=base_url('add-rt')?>" method="post">
                    <fieldset class="form-group">
                        <label>Dusun</label>
                        <select class="form-control" name="dusun_id" onchange="getRw(this)">
                            <option selected="" value="" disabled=""> .:: Pilih Dusun ::.</option>
                            <?php foreach ($dusun->result() as $key) { ?>
                                <option value="<?=$key->id_dusun?>"><?=$key->nama_dusun;?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nomor RW</label>
                        <select class="form-control" name="rw_id" id="rw_id">
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nomor RT</label>
                        <input type="text" autocomplete="off" class="form-control" name="nomor_rt" placeholder="masukkan nomor RT">
                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nama Ketua RT</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_ketua" placeholder="masukkan nama ketua RT">
                    </fieldset>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?=base_url('data-rt');?>" class="btn btn-secondary">Kembali</a>
                </form>
            </div>
        </div>
    </div>
<br>
</div>
<script type="text/javascript">
    function getRw(ini){
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('get-rw')?>',
            async : true,
            dataType : 'json',
            data: {id_dusun:ini.value},
            success: function(response) { 
                if(response.status == 'success'){
                  var html = '<option value="">.:: Nomor RW ::.</option>';
                  $.each(response.data,function(k,v){
                    html += '<option value="'+v.id_rw+'">'+v.nomor_rw+'</option>';
                  });
                  $('#rw_id').html(html);
                  // $('#rw_id').trigger("chosen:updated");
                } else {
                  return "";
                }
            }
        });
    } 
</script>