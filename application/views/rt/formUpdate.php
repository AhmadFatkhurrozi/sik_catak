<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form action="<?=base_url('update-rt')?>" method="post">
                    <input type="hidden" name="id" value="<?=$data->id_rt;?>">
                    <fieldset class="form-group">
                        <label>Dusun</label>
                        <select class="form-control" name="dusun_id" id="dusun_id" onchange="getRw()">
                            <option selected="" value="" disabled=""> .:: Pilih Dusun ::.</option>
                            <?php foreach ($dusun->result() as $key) { ?>
                                <option value="<?=$key->id_dusun?>" <?php if($key->id_dusun == $data->dusun_id) echo "selected"; ?>><?=$key->nama_dusun;?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nomor RW</label>
                        <select class="form-control" name="rw_id" id="rw_id">
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nomor RT</label>
                        <input type="text" autocomplete="off" class="form-control" name="nomor_rt" value="<?=$data->nomor_rt;?>">
                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nama Ketua RT</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_ketua" value="<?=$data->nama_ketua_rt;?>">
                    </fieldset>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?=base_url('data-rt');?>" class="btn btn-secondary">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    <br>
</div>
<script type="text/javascript">
   $(document).ready(function() {
        getRw();
    });
    
    function getRw(){
        var dusun = $('#dusun_id').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('get-rw')?>',
            async : true,
            dataType : 'json',
            data: {id_dusun:dusun},
            success: function(response) { 
                if(response.status == 'success'){
                  var html = '<option value="">.:: Nomor RW ::.</option>';
                  $.each(response.data,function(k,v){
                    html += '<option value="'+v.id_rw+'">'+v.nomor_rw+'</option>';
                  });
                  $('#rw_id').html(html);
                  // $('#rw_id').trigger("chosen:updated");
                } else {
                  return "";
                }
            }
        });
    } 

</script>