<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Kependudukan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php 
  		echo __css('fontawesome');
  		// echo __css('bootstrap');

    ?>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <?php
  		echo __css('fontastic');
  		echo __css('default');
  		echo __css('custom');
      // echo __js('jquery');
      // echo __js('popper'); 
    ?>
    <script src="https://rpl.jokerindonesia.com/plugins/jquery/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


    <?php
      // echo __js('bootstrap');
      echo __js('checkall');
      echo __js('_checkall');
     ?>
    
     <link rel="stylesheet" type="text/css" href="<?=base_url('/dist/datatable/dataTables.bootstrap4.min.css')?>">
    <script src="<?=base_url('/dist/datatable/jquery.dataTables.min.js')?>"></script>
    <script src="<?=base_url('/dist/datatable/dataTables.bootstrap4.min.js')?>"></script>
    <!-- <script src="<?=base_url('/dist/js/highcharts.js')?>"></script> -->
    <script src="https://code.highcharts.com/highcharts.js"></script>

    
    <style type="text/css">
      .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background-color: #fff;
      }
      .preloader .loading {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
        font: 14px arial;
      }
      ul li a {
        color: #000 !important;
      }
      nav.side-navbar{
        /*background-color: rgb(0 0 0 / 68%) !important;*/
      }
      table tbody tr td{
        color: #000 !important;
      }
      .list-group-item {
        border: 0 !important;
      }
      html, body {
         margin:0;
         padding:0;
         height:100%;
      }

      .page{
        position:relative;
        min-height: 100%;
      }

      .highcharts-credits{
        display: none !important;
      }
    </style>

  </head>
  <body>
    <div class="page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar">
          <!-- Search Box-->
          <div class="search-box">
            <button class="dismiss"><i class="icon-close"></i></button>
            <form id="searchForm" action="#" role="search">
              <input type="search" placeholder="What are you looking for..." class="form-control">
            </form>
          </div>
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand -->
                <a href="<?=base_url('admin/')?>" class="navbar-brand d-none d-sm-inline-block">
                  <div class="brand-text d-none d-lg-inline-block"><span>Admin </span><strong> SIK Catak Gayam</strong></div>
                </a>
                <a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                <li class="nav-item">
                  <a href="<?=base_url('login/logout')?>" class="nav-link logout text-light" onclick="return confirm('Keluar ?')"> 
                    <span class="d-none d-sm-inline">Logout</span><i class="fa fa-sign-out"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
         <!--  <div class="sidebar-header d-flex align-items-center" style="margin-bottom: -40px;">
            <div class="title">
              <h1 class="h5"><?php echo $this->session->userdata('get_nama'); ?></h1>
              <?php ($this->session->userdata('get_level')) == "admin" ? $lvl="Admin" : $lvl="Petugas";?>
              <p><?=$lvl;?></p>
            </div>
          </div> -->
          <ul class="list-unstyled">
            <li>
              <a href="<?=base_url('admin')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Dashboard', $judul ); ?>"><i class="fa fa-dashboard"></i> Dashboard 
              </a>
            </li>
            <li>
              <a href="<?=base_url('data-penduduk')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data Penduduk', $judul ); ?>">
                  <i class="fa fa-list-alt"></i><span>Data Penduduk</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('data-kk')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data Kepala Keluarga', $judul ); ?>">
                  <i class="fa fa-list-alt"></i><span>Data Kepala Keluarga</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('export-penduduk')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Export Data Penduduk', $judul ); ?>">
                  <i class="fa fa-file-excel-o"></i><span>Export Data Penduduk</span>
              </a>
            </li>

            <div class="sidebar-header d-flex align-items-center" style="padding: 15px 15px 0px 15px;">
              <div class="title">
                <p>Peristiwa</p>
              </div>
            </div>
           <!--  <li>
              <a href="" class="btn-sm lst list-group-item <?php echo menu_aktif('Pindah Penduduk', $judul ); ?>">
                  <i class="fa fa-exchange"></i><span>Pindah Penduduk</span>
              </a>
            </li> -->
            <li>
              <a href="<?=base_url('penduduk-meninggal')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data Penduduk Meninggal', $judul ); ?>">
                  <i class="fa fa-ambulance"></i><span>Meninggal</span>
              </a>
            </li>

            <div class="sidebar-header d-flex align-items-center" style="padding: 15px 15px 0px 15px;">
              <div class="title">
                <p>Surat Kependudukan</p>
              </div>
            </div>

            <li>
              <a href="<?=base_url('admin-surat-kematian')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Surat Kematian', $judul ); ?>">
                  <i class="fa fa-envelope"></i><span>Surat Kematian</span>
              </a>
            </li>

            <li>
              <a href="<?=base_url('admin-surat-kelahiran')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Surat Kelahiran', $judul ); ?>">
                  <i class="fa fa-envelope"></i><span>Surat Kelahiran</span>
              </a>
            </li>

            <!-- <li>
              <a href="<?=base_url('admin-surat-pindah')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Surat Pindah', $judul ); ?>">
                  <i class="fa fa-envelope"></i><span>Surat Pindah</span>
              </a>
            </li>-->

            <li>
              <a href="<?=base_url('admin-surat-nikah')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Pengantar Nikah', $judul ); ?>">
                  <i class="fa fa-envelope"></i><span>Pengantar Nikah</span>
              </a>
            </li> 

            <div class="sidebar-header d-flex align-items-center" style="padding: 15px 15px 0px 15px;">
              <div class="title">
                <p>Surat Keterangan</p>
              </div>
            </div>
            
            <li>
              <a href="<?=base_url('admin-skck')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('SKCK', $judul ); ?>">
                  <i class="fa fa-envelope"></i><span>SKCK</span>
              </a>
            </li>

            <li>
              <a href="<?=base_url('admin-sktm')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Surat Ketarangan Tidak Mampu', $judul ); ?>">
                  <i class="fa fa-envelope"></i><span>SKTM</span>
              </a>
            </li>
            
            <?php if($this->session->userdata('get_level') == 'admin') { ?>
            <div class="sidebar-header d-flex align-items-center" style="padding: 15px 15px 0px 15px;">
              <div class="title">
                <p>Data Master</p>
              </div>
            </div>
            
            <li>
              <a href="<?=base_url('manage-pengguna')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data Pengguna', $judul ); ?>">
                  <i class="fa fa-users"></i><span>Pengguna</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('data-dusun')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data Dusun', $judul ); ?>">
                  <i class="fa fa-list-alt"></i><span>Data Dusun</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('data-rw')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data RW', $judul ); ?>">
                  <i class="fa fa-list-alt"></i><span>Data RW</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('data-rt')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Data RT', $judul ); ?>">
                  <i class="fa fa-list-alt"></i><span>Data RT</span>
              </a>
            </li>
            <li>
              <a href="<?=base_url('profil-desa')?>" class="btn-sm lst list-group-item <?php echo menu_aktif('Profil Desa', $judul ); ?>">
                  <i class="fa fa-cog"></i><span>Profil Desa</span>
              </a>
            </li>
            <?php } ?>

          </ul>

        
        </nav>
        <div class="content-inner"> 

          <?php echo $konten; ?>

          <footer class="main-footer">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12 text-right">
                  <p>Novi &copy; 2020</p>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>

   <script>
    $(document).ready(function(){
    $(".preloader").fadeOut();
    })
   </script>

   <script type="text/javascript">
     $('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('active');
        $(document).trigger('sidebarChanged');

        if ($(window).outerWidth() > 1183) {
            if ($('#toggle-btn').hasClass('active')) {
                $('.navbar-header .brand-small').hide();
                $('.navbar-header .brand-big').show();
            } else {
                $('.navbar-header .brand-small').show();
                $('.navbar-header .brand-big').hide();
            }
        }

        if ($(window).outerWidth() < 1183) {
            $('.navbar-header .brand-small').show();
        }
    });
   </script>
  </body>
</html>