<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi Kependudukan</title>

    <?php 
        echo __css('bootstrap');
        // echo __css('theme');
        echo __css('template');
        echo __css('fontawesome');
        echo __css('fontastic');
        echo __css('default');
        echo __css('custom');
    ?>

    <style type="text/css">
      .cover-container {
          height: 100vh;
      }

      .flex-fill {
          flex: 1 1 auto;
      }

      .bg-img{
        background-image: url('./dist/img/bg.png');
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
      }

      .card-img-top{
        width: 50% !important;
        text-align: center !important;
        padding-top: 10px !important;
      }
      
      .pilihan{
        text-decoration: none !important;
      }

      .card{
        background: #FFF9;
        border-radius: 10px;
      }

      .card-body{
        padding: 0rem !important;
      }
    </style>
    
    <link rel="stylesheet" href="<?=base_url('/dist/owl/owl.carousel.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('/dist/owl/owl.theme.default.min.css');?>">
  </head>
  
  <body class="bg-img">
    <div class="cover-container text-center d-flex flex-column main-layer">
        <div class="row align-items-center justify-content-center flex-fill main-layer">
            <div class="col-12 col-lg-12 col-md-12 mt-4">
                <main>
                  <center><img src="<?=base_url('/dist/img/logo-jombang.png');?>" height="80px"></center>
                  <h1 class="text-light p-3" style="font-size: 32px; text-shadow: 0.1px 1px #000;">Sistem Informasi Kependudukan</h1>
                  <h2 class="text-light" style="text-shadow: 0.1px 1px #000;">Desa Catak Gayam</h2>
                  <br class="clearfix py-3">
                  
                  <div class="container">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="card text-center mx-3">
                          <a href="<?=base_url('site/penduduk');?>" class="pilihan">
                            <img class="card-img-top" src="<?=base_url('dist/img/dokumen.png');?>" alt="">
                            <div class="card-body">
                              <h4 class="card-title text-dark">Data Penduduk</h4>
                            </div>
                          </a>
                        </div>
                      </div>
                      
                      <div class="col-md-3">
                        <div class="card text-center mx-3">
                          <a href="<?=base_url('site/statistik');?>" class="pilihan">
                            <img class="card-img-top" src="<?=base_url('dist/img/statistik.png');?>" alt="">
                            <div class="card-body">
                              <h4 class="card-title text-dark">Statistik</h4>
                            </div>
                          </a>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="card text-center mx-3">
                          <a href="<?=base_url('site/surat');?>" class="pilihan">
                            <img class="card-img-top" src="<?=base_url('dist/img/usulan.png');?>" alt="">
                            <div class="card-body">
                              <h4 class="card-title text-dark">Pengajuan Surat</h4>
                            </div>
                          </a>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="card text-center mx-3">
                          <a href="<?=base_url('site/profil');?>" class="pilihan">
                            <img class="card-img-top" src="<?=base_url('dist/img/kajian.png');?>" alt="">
                            <div class="card-body">
                              <h4 class="card-title text-dark">Profil Desa</h4>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </main>
            </div>
        </div>
        <div class="row align-items-center justify-content-center flex-fill other-page"></div>
        
          <footer style="background: rgba(0,0,0,.24) !important; ">
              Novi 2020
          </footer>
      </div>


    <?php
        echo __js('jquery'); 
        echo __js('popper');
        echo __js('bootstrap');
     ?>

     <script type="text/javascript">
       function statistik() {
        $('.main-layer').hide();
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('statistik')?>',
          async : true,
          dataType : 'json',
          data: {},
          success: function(response) { 
            if(response.status == 'success'){
              $('.other-page').html(response.content).fadeIn();
            } else {
              $('.main-layer').show();
            }
          }
        });
       }

       function dataPenduduk() {
        $('.main-layer').hide();
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('penduduk')?>',
          async : true,
          dataType : 'json',
          data: {},
          success: function(response) { 
            if(response.status == 'success'){
              $('.other-page').html(response.content).fadeIn();
            } else {
              $('.main-layer').show();
            }
          }
        });
       }

       function surat() {
        $('.main-layer').hide();
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('surat')?>',
          async : true,
          dataType : 'json',
          data: {},
          success: function(response) { 
            if(response.status == 'success'){
              $('.other-page').html(response.content).fadeIn();
            } else {
              $('.main-layer').show();
            }
          }
        });
       }

       function profil() {
        $('.main-layer').hide();
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('profil')?>',
          async : true,
          dataType : 'json',
          data: {},
          success: function(response) { 
            if(response.status == 'success'){
              $('.other-page').html(response.content).fadeIn();
            } else {
              $('.main-layer').show();
            }
          }
        });
       }
     </script>
</body>
</html>