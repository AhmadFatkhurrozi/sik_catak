<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th>ID RT</th>
                <th>Nomor RT</th>
                <th>ID RW</th>
                <th>Nomor RW</th>
                <th>ID Dusun</th>
                <th>Nama Dusun</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($data->result() as $a) { ?>
            <tr>
                <td align="center"><?=$a->id_rt;?></td>
                <td align="center"><?=$a->nomor_rt;?></td>
                <td align="center"><?=$a->id_rw;?></td>
                <td align="center"><?=$a->nomor_rw;?></td>
                <td align="center"><?=$a->id_dusun;?></td>
                <td align="center"><?=$a->nama_dusun;?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>