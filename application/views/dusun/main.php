<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <a href="<?=base_url('form-dusun')?>" class="btn btn-primary btn-sm">Tambah Data</a>
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>Nama Dusun</th>
                <th style="text-align: center;">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach ($data->result() as $a) { ?>
            <tr>
                <td align="center"><?=$no++; ?></td>
                <td><?=$a->nama_dusun;?></td>
                <td align="center">
                    <a href="<?=base_url('edit-dusun/'.$a->id_dusun)?>" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> edit</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>