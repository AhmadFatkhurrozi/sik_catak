<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form action="<?=base_url('add-rw')?>" method="post">
                    <fieldset class="form-group">
                        <label>Dusun</label>
                        <select class="form-control" name="dusun_id">
                            <option selected="" value="" disabled=""> .:: Pilih Dusun ::.</option>
                            <?php foreach ($dusun->result() as $key) { ?>
                                <option value="<?=$key->id_dusun?>"><?=$key->nama_dusun;?></option>
                            <?php } ?>
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nomor RW</label>
                        <input type="text" autocomplete="off" class="form-control" name="nomor_rw" placeholder="masukkan nomor RW">
                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Nama Ketua RW</label>
                        <input type="text" autocomplete="off" class="form-control" name="nama_ketua" placeholder="masukkan nama ketua RW">
                    </fieldset>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?=base_url('data-rw');?>" class="btn btn-secondary">Kembali</a>
                </form>
            </div>
        </div>
    </div>
<br>
</div>