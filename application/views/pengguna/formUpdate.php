<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header>
<br>   
<div class="container-fluid">
    <div class="card card-body">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form action="<?=base_url('update-pengguna')?>" method="post">
                    <input type="hidden" name="id" value="<?=$pengguna->id;?>">
                    <fieldset class="form-group">
                        <label>Username</label>
                        <input type="text" autocomplete="off" class="form-control" name="username" placeholder="masukkan username" value="<?=$pengguna->username;?>" readonly="">
                        <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Password</label>
                        <input type="password" autocomplete="off" name="password" class="form-control" placeholder="kosongi jika tidak ingin mengubah password">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Nama</label>
                        <input type="text" autocomplete="off" name="nama" value="<?=$pengguna->nama;?>" class="form-control" placeholder="masukkan nama lengkap">
                    </fieldset>

                    <fieldset class="form-group">
                        <label>Level</label>
                        <label class="radio-inline">
                            <input type="radio" name="level" <?=($pengguna->level == "admin") ? "checked" : "";?> value="admin"> Admin
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="level" <?=($pengguna->level == "petugas") ? "checked" : "";?> value="petugas"> Petugas
                        </label>
                    </fieldset>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="<?=base_url('manage-pengguna');?>" class="btn btn-secondary">Kembali</a>
                </form>
            </div>
        </div>
    </div>
    <br>
</div>