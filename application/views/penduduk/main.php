<header class="page-header">
    <div class="container-fluid">
      <h2 class="no-margin-bottom"><?php echo $judul; ?></h2>
    </div>
</header> 
<br> 
<div class="container-fluid">   
    <a href="<?=base_url('form-penduduk')?>" class="btn btn-primary btn-sm">Tambah Data</a>
    <p id="notifications"><?php echo $this->session->flashdata('msg'); ?></p>
    <table class="table table-bordered table-hover table-sm" id="datatable">
        <thead class="bg-primary text-light">
            <tr>
                <th width="35px">No</th>
                <th>Nama</th>
                <th style="text-align: center;">Opsi</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );
</script>