<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	function auth($username, $pass)
	{
		$this->db->where('username', $username);
		$this->db->where('password',md5($pass));
		$query = $this->db->get('users');
		return $query;
	}

	function auth_penduduk($nik)
	{
		$this->db->where('nik', $nik);
		$query = $this->db->get('penduduk');
		return $query;
	}
	
	function tampil_data($table){
		return $this->db->get($table);
	}

	function hapus_data($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	function create($table,$data)
	{
    	$query = $this->db->insert($table, $data);
    	return $query;
	}

	function ubah_data($where,$data,$table)
	{
		$this->db->where($where);
		$this->db->update($table,$data);
	}

}

/* End of file M_data.php */
/* Location: ./application/models/M_data.php */