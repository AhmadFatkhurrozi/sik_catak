<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//ekspor xls
function xlsBOF()
{
    echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
    return;
}

function xlsEOF()
{
    echo pack("ss", 0x0A, 0x00);
    return;
}

function xlsWriteNumber($Row, $Col, $Value)
{
    echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
    echo pack("d", $Value);
    return;
}

function xlsWriteLabel($Row, $Col, $Value)
{
    $L = strlen($Value);
    echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
    echo $Value;
    return;
}

function format_rupiah($num)
{

    return number_format(str_replace('.', '', $num), 0, '', '.');
}

function tanggal($tanggal = '', $jat = '')
{
    if ($jat == 'jam') {
        return date_format(date_create($tanggal), 'd-m-Y H:i:s');
    } else {
        return date_format(date_create($tanggal), 'd-m-Y');
    }
}

function export_excel($var = '', $nama_file)
{
    if ($var) {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=" . $nama_file . ".xls");
    }
}

function DMStoDD($str)
{

    $valarr = explode(" ", $str);
    $deg =  (int) str_replace("°", "", str_replace("º", '', $valarr[0]));
    $min = (int) str_replace('.', '', str_replace("'", '', $valarr[1]));
    $h = '';
    if (strlen($min / 6000) < 7) {

        if (strlen($min / 6000) < 6) {
            $h = (($min / 6000) . '01');
        } else {
            $h = (($min / 6000) . '1');
        }
    } else {
        $h = ($min / 6000);
    }
    $ok = (strlen((int)$deg . str_replace('.', '', round($h, 5))) < 7) ? ((int)$deg . str_replace('.', '', round($h, 5))) . '1' : ((int)$deg . str_replace('.', '', round($h, 5)));
    return  $ok / 1000000;
}

function DDtoDMS($dec)
{
    // Converts decimal format to DMS ( Degrees / minutes / seconds ) 
    $vars = explode(".", $dec);
    $deg = $vars[0];
    $tempma = "0." . $vars[1];

    $tempma = $tempma * 3600;
    $min = round(($tempma / 60), 3);
    // $sec = $tempma - ($min * 60);

    return $deg . "°" . $min . "'";
}

// hitung jarak dengan koordinat
function distance(
    $latitudeFrom,
    $longitudeFrom,
    $latitudeTo,
    $longitudeTo,
    // $earthRadius = 6371000 aslinya
    $earthRadius = 6378160
) {
    // convert from degrees to radians
    $latFrom = deg2rad((int)$latitudeFrom);
    $lonFrom = deg2rad((int)$longitudeFrom);
    $latTo = deg2rad((int)$latitudeTo);
    $lonTo = deg2rad((int)$longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
}
function format_angka($number = '')
{
    return  number_format($number, '0', ',', '.');
}

function haversineGreatCircleDistance(
    $latitudeFrom,
    $longitudeFrom,
    $latitudeTo,
    $longitudeTo,
    $earthRadius = 6371000
) {
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return $angle * $earthRadius;
}

function hitungjarak($kandanglat, $kandanglng, $lepasanlat, $lepasanlng)
{
    $korleplat = $lepasanlat;
    $korleplng = $lepasanlng;

    $lat = $kandanglat;
    $lng = $kandanglng;


    $pi = 3.141592654;
    $const1 = 0.996647189;
    $const2 = 6378137;
    $pilat    = $korleplat * $pi / 180;
    $pilng    = $korleplng * $pi / 180;


    $k1 = $lat * $pi / 180;
    $k2 = $lng * $pi / 180;
    $k3 = ($k1 + $pilat) / 2;
    $k4 = $pilng - $k2;
    $k5 = 0.0067394967422767 * pow(COS($k3), 2);
    $k6 = sqrt(1 + $k5);
    $k7 = $k6 * $k4;
    $k8 = atan($const1 * tan($k1));
    $k9 = atan($const1 * tan($pilat));
    $k10 = sin($k8) * sin($k9) + cos($k8) * cos($k9) * cos($k7);
    return $jarak = ($const2 / $k6) * (atan(-$k10 / sqrt(1 - pow($k10, 2))) + 2 * atan(1));
}
