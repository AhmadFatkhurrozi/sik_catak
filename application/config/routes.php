<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'front';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['admin']         = 'MenuAdmin/Dashboard';
$route['dashboard']         = 'MenuAdmin/Dashboard';


$route['manage-pengguna'] = 'MenuAdmin/Pengguna';
$route['form-pengguna'] = 'MenuAdmin/Pengguna/formAdd';
$route['add-pengguna'] = 'MenuAdmin/Pengguna/add';
$route['edit-pengguna/:any'] = 'MenuAdmin/Pengguna/formEdit';
$route['update-pengguna'] = 'MenuAdmin/Pengguna/update';
$route['hapus-pengguna/:any'] = 'MenuAdmin/Pengguna/hapus';


$route['data-penduduk'] = 'MenuAdmin/Penduduk';
$route['data-kk'] = 'MenuAdmin/Penduduk/KepalaKeluarga';
$route['form-penduduk']   = 'MenuAdmin/Penduduk/formAdd';
$route['import-penduduk']   = 'MenuAdmin/Penduduk/import';
$route['import-file']   = 'MenuAdmin/Penduduk/importFile';
$route['do-import-penduduk']   = 'MenuAdmin/Penduduk/doimportFile';
$route['add-penduduk']   = 'MenuAdmin/Penduduk/add';
$route['edit-penduduk/:any']   = 'MenuAdmin/Penduduk/formEdit';
$route['detail-penduduk/:any']   = 'MenuAdmin/Penduduk/detail';
$route['detail-keluarga/:any']   = 'MenuAdmin/Penduduk/detailKeluarga';
$route['update-penduduk']   = 'MenuAdmin/Penduduk/update';
$route['getDataPenduduk']   = 'MenuAdmin/Penduduk/getDataPenduduk';

$route['export-penduduk'] = 'MenuAdmin/Eksport';


$route['kamus']   = 'MenuAdmin/DataDusun/kamus';
$route['data-dusun']   = 'MenuAdmin/DataDusun';
$route['form-dusun']   = 'MenuAdmin/DataDusun/formAdd';
$route['add-dusun']   = 'MenuAdmin/DataDusun/add';
$route['edit-dusun/:any']   = 'MenuAdmin/DataDusun/formEdit';
$route['update-dusun']   = 'MenuAdmin/DataDusun/update';

$route['data-rw']      = 'MenuAdmin/DataRw';
$route['form-rw']   = 'MenuAdmin/DataRw/formAdd';
$route['add-rw']   = 'MenuAdmin/DataRw/add';
$route['edit-rw/:any']   = 'MenuAdmin/DataRw/formEdit';
$route['update-rw']   = 'MenuAdmin/DataRw/update';

$route['data-rt']      = 'MenuAdmin/DataRt';
$route['form-rt']   = 'MenuAdmin/DataRt/formAdd';
$route['add-rt']   = 'MenuAdmin/DataRt/add';
$route['edit-rt/:any']   = 'MenuAdmin/DataRt/formEdit';
$route['update-rt']   = 'MenuAdmin/DataRt/update';
$route['get-rw']   = 'MenuAdmin/DataRt/getRw';
$route['get-rt']   = 'MenuAdmin/DataRt/getRt';

$route['profil-desa']      = 'MenuAdmin/ProfilDesa';
$route['edit-profil/:any']   = 'MenuAdmin/ProfilDesa/formEdit';
$route['update-profil']   = 'MenuAdmin/ProfilDesa/update';

$route['cari-penduduk']   = 'site/Penduduk/cariPenduduk';

$route['cek-nik']   = 'site/Surat/cekNik';
$route['pembuatan-surat']   = 'site/Surat/pilihSurat';

$route['surat-kematian']   = 'MasterSurat/Kematian';
$route['simpan-kematian']   = 'MasterSurat/Kematian/simpan';
$route['surat-kematian/:any']   = 'MasterSurat/Kematian/detailSurat';
$route['cetak-kematian/:any']   = 'MasterSurat/Kematian/cetak';

$route['penduduk-meninggal']   = 'MenuAdmin/Meninggal';
$route['form-meninggal']   = 'MenuAdmin/Meninggal/formAdd';


$route['skck']   = 'MasterSurat/Skck';
$route['simpan-skck']   = 'MasterSurat/Skck/simpan';
$route['skck/:any']   = 'MasterSurat/Skck/detailSurat';
$route['cetak-skck/:any']   = 'MasterSurat/Skck/cetak';

$route['sktm']   = 'MasterSurat/Sktm';
$route['simpan-sktm']   = 'MasterSurat/Sktm/simpan';
$route['sktm/:any']   = 'MasterSurat/Sktm/detailSurat';
$route['cetak-sktm/:any']   = 'MasterSurat/Sktm/cetak';

$route['surat-pindah']   = 'MasterSurat/SuratPindah';
$route['simpan-surat-pindah']   = 'MasterSurat/SuratPindah/simpan';
$route['surat-pindah/:any']   = 'MasterSurat/SuratPindah/detailSurat';
$route['cetak-surat-pindah/:any']   = 'MasterSurat/SuratPindah/cetak';

$route['surat-nikah']   = 'MasterSurat/SuratNikah';
$route['simpan-surat-nikah']   = 'MasterSurat/SuratNikah/simpan';
$route['surat-nikah/:any']   = 'MasterSurat/SuratNikah/detailSurat';
$route['cetak-surat-nikah/:any']   = 'MasterSurat/SuratNikah/cetak';

$route['surat-kelahiran']   = 'MasterSurat/SuratKelahiran';
$route['simpan-surat-kelahiran']   = 'MasterSurat/SuratKelahiran/simpan';
$route['surat-kelahiran/:any']   = 'MasterSurat/SuratKelahiran/detailSurat';
$route['cetak-surat-kelahiran/:any']   = 'MasterSurat/SuratKelahiran/cetak';

$route['admin-surat-kematian']   = 'MenuAdmin/Surat/kematian';
$route['admin-skck']   = 'MenuAdmin/Surat/skck';
$route['admin-sktm']   = 'MenuAdmin/Surat/sktm';
$route['admin-surat-kelahiran']   = 'MenuAdmin/Surat/kelahiran';
$route['admin-surat-pindah']   = 'MenuAdmin/Surat/pindah';
$route['admin-surat-nikah']   = 'MenuAdmin/Surat/nikah';
