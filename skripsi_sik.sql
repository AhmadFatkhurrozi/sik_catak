-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2021 at 07:37 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_sik`
--

-- --------------------------------------------------------

--
-- Table structure for table `dusun`
--

CREATE TABLE `dusun` (
  `id_dusun` int(11) NOT NULL,
  `nama_dusun` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dusun`
--

INSERT INTO `dusun` (`id_dusun`, `nama_dusun`) VALUES
(1, 'Catak Gayam Selatan'),
(2, 'Catak Gayam Utara'),
(3, 'Tawangsari');

-- --------------------------------------------------------

--
-- Table structure for table `penduduk`
--

CREATE TABLE `penduduk` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `no_kk` varchar(100) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `jk` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `agama` enum('Islam','Kristen Protestan','Kristen Katolik','Hindu','Buddha','Khonghucu') DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `status_kawin` varchar(255) DEFAULT NULL,
  `status_keluarga` varchar(255) DEFAULT NULL,
  `kwarganegaraan` varchar(255) DEFAULT NULL,
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `dusun_id` int(10) UNSIGNED DEFAULT NULL,
  `dusun` varchar(50) DEFAULT NULL,
  `rw_id` int(10) UNSIGNED DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `rt_id` int(10) UNSIGNED DEFAULT NULL,
  `rt` varchar(10) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penduduk`
--

INSERT INTO `penduduk` (`id`, `nama`, `no_kk`, `nik`, `jk`, `tgl_lahir`, `tempat_lahir`, `agama`, `pekerjaan`, `status_kawin`, `status_keluarga`, `kwarganegaraan`, `nama_ayah`, `nama_ibu`, `dusun_id`, `dusun`, `rw_id`, `rw`, `rt_id`, `rt`, `status`) VALUES
(10, 'BUDI SUROSO', '3517070102100002', '3517070711780000', 'Laki-laki', '1978-07-11', 'MOJOKERTO', 'Islam', 'WIRASWASTA', 'Kawin', 'Kepala Keluarga', 'WNI', 'TOYIB', 'TARWIYAH', 2, 'Catak Gayam Utara', 6, '004', 11, '002', 'tinggal ditempat'),
(11, 'ENI SHOFA', '3517070102100002', '3517074101850010', 'Perempuan', '1985-01-01', 'JOMBANG', 'Islam', 'KARYAWAN SWASTA', 'Kawin', 'Istri', 'WNI', 'SUPARNO', 'MAKIYAH', 2, 'Catak Gayam Utara', 6, '005', 11, '003', 'tinggal ditempat'),
(13, 'SYIFA\'', '3517070107100003', '3517072302800000', 'Laki-laki', '1980-02-23', 'JOMBANG', 'Islam', 'KARYAWAN SWASTA', 'Kawin', 'Kepala Keluarga', 'WNI', 'AMZAT', 'FATIMAH', 2, 'Catak Gayam Utara', 6, '004', 11, '002', 'tinggal ditempat'),
(14, 'LUTFIYAH', '3517070107100003', '3517076212840000', 'Perempuan', '1984-12-22', 'JOMBANG', 'Islam', 'MENGURUS RUMAH TANGGA', 'Kawin', 'Istri', 'WNI', 'YASIR', 'KHASANAH', 2, 'Catak Gayam Utara', 6, '005', 11, '003', 'tinggal ditempat'),
(15, 'AHMAD FARUQ', '3517070107100003', '3517072205100000', 'Laki-laki', '2010-05-22', 'JOMBANG', 'Islam', 'BELUM/TIDAK BEKERJA', 'Belum kawin', 'Anak', 'WNI', 'SYIFA\'', 'LUTFIYAH', 2, 'Catak Gayam Utara', 6, '006', 11, '004', 'tinggal ditempat'),
(27, 'SUPARDI', '3517070112070061', '3517070409680000', 'Laki-laki', '1968-09-04', 'JOMBANG', 'Islam', 'PETANI/PEKEBUN', 'Kawin', 'Kepala Keluarga', 'WNI', 'WAGIRAN', 'KARTIN', 2, 'Catak Gayam Utara', 6, '004', 11, '002', 'tinggal ditempat'),
(28, 'SITI WAQIAH', '3517070112070061', '3517077006790000', 'Perempuan', '1979-06-30', 'JOMBANG', 'Islam', 'MENGURUS RUMAH TANGGA', 'Kawin', 'Istri', 'WNI', 'SAMI\'AN', 'NGATINI', 2, 'Catak Gayam Utara', 6, '005', 11, '003', 'tinggal ditempat'),
(29, 'NUR KUMALA SARI', '3517070112070061', '3517075712990000', 'Perempuan', '1999-12-17', 'JOMBANG', 'Islam', 'BELUM/TIDAK BEKERJA', 'Belum kawin', 'Anak', 'WNI', 'SUPARDI', 'SITI WAQIAH', 2, 'Catak Gayam Utara', 6, '006', 11, '004', 'tinggal ditempat'),
(30, 'MUHAMMAD FAISAL', '3517070112070061', '3517072202040000', 'Laki-laki', '2004-02-22', 'JOMBANG', 'Islam', 'BELUM/TIDAK BEKERJA', 'Belum kawin', 'Anak', 'WNI', 'SUPARDI', 'SITI WAQIAH', 2, 'Catak Gayam Utara', 6, '007', 11, '005', 'tinggal ditempat'),
(31, 'IMAM MUJTAHIDIN', '3517070112070062', '3517071711680000', 'Laki-laki', '1968-11-17', 'JOMBANG', 'Islam', 'TUKANG KAYU', 'Kawin', 'Kepala Keluarga', 'WNI', 'AMINUN', 'MUNIAH', 2, 'Catak Gayam Utara', 6, '008', 11, '006', 'tinggal ditempat'),
(32, 'ROIKHANNAH', '3517070112070062', '3517074503720000', 'Perempuan', '1972-03-05', 'MOJOKERTO', 'Islam', 'MENGURUS RUMAH TANGGA', 'Kawin', 'Istri', 'WNI', 'MUNODO', 'DAUWAMAH', 2, 'Catak Gayam Utara', 6, '009', 11, '007', 'tinggal ditempat'),
(33, 'ZUMROTUL FARIKHAH', '3517070112070062', '3517074403970000', 'Perempuan', '1997-03-04', 'JOMBANG', 'Islam', 'PELAJAR/MAHASISWA', 'Belum kawin', 'Anak', 'WNI', 'IMAM MUZTAHIDIN', 'ROICHANNAH', 2, 'Catak Gayam Utara', 6, '010', 11, '008', 'tinggal ditempat'),
(34, 'ARDIK AULIYA ROHMAN', '3517070112070062', '3517070505000000', 'Laki-laki', '2000-05-05', 'JOMBANG', 'Islam', 'PELAJAR/MAHASISWA', 'Belum kawin', 'Anak', 'WNI', 'IMAM MUZTAHIDIN', 'ROICHANNAH', 2, 'Catak Gayam Utara', 6, '011', 11, '009', 'tinggal ditempat'),
(35, 'INDAH MIFTACHUL JANNAH', '3517070112070062', '3517074111060010', 'Perempuan', '2006-11-01', 'JOMBANG', 'Islam', 'BELUM/TIDAK BEKERJA', 'Kawin', 'Anak', 'WNI', 'IMAM MUZTAHIDIN', 'ROICHANNAH', 2, 'Catak Gayam Utara', 6, '012', 11, '010', 'tinggal ditempat');

-- --------------------------------------------------------

--
-- Table structure for table `pengantar_nikah`
--

CREATE TABLE `pengantar_nikah` (
  `id_pengantar_nikah` int(11) NOT NULL,
  `nik` varchar(100) DEFAULT NULL,
  `no_surat` varchar(100) DEFAULT NULL,
  `status_kawin` varchar(100) DEFAULT NULL,
  `istri_ke` int(11) DEFAULT NULL,
  `nama_mantan` varchar(100) DEFAULT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `nik_ayah` varchar(100) DEFAULT NULL,
  `nik_ibu` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengantar_nikah`
--

INSERT INTO `pengantar_nikah` (`id_pengantar_nikah`, `nik`, `no_surat`, `status_kawin`, `istri_ke`, `nama_mantan`, `tgl_dibuat`, `nik_ayah`, `nik_ibu`) VALUES
(1, '3517075712990000', '400/1/415.17.14/2021', 'Perawan', NULL, NULL, '2021-02-06', '3517070409680000', '3517077006790000');

-- --------------------------------------------------------

--
-- Table structure for table `profil_desa`
--

CREATE TABLE `profil_desa` (
  `id_profil_desa` int(11) NOT NULL,
  `kepala_desa` varchar(100) DEFAULT NULL,
  `sekdes` varchar(100) DEFAULT NULL,
  `kaur_keuangan` varchar(100) DEFAULT NULL,
  `kaur_pemerintahan` varchar(100) DEFAULT NULL,
  `kaur_pembangunan` varchar(100) DEFAULT NULL,
  `kasi_kesra` varchar(100) DEFAULT NULL,
  `kaur_umum` varchar(100) DEFAULT NULL,
  `kasun_utara` varchar(100) DEFAULT NULL,
  `kasun_selatan` varchar(100) DEFAULT NULL,
  `kasun_tawangsari` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `profil_desa`
--

INSERT INTO `profil_desa` (`id_profil_desa`, `kepala_desa`, `sekdes`, `kaur_keuangan`, `kaur_pemerintahan`, `kaur_pembangunan`, `kasi_kesra`, `kaur_umum`, `kasun_utara`, `kasun_selatan`, `kasun_tawangsari`) VALUES
(1, 'Sugeng', 'Asmunin', 'Inayatul Khasanah', 'Ahmad Mubin', 'Ita Imyeni', 'Abdul Rozaq', 'Zulaikhah', 'Imam Sofii', 'Lukman Fauzi', 'Imam Mujayin');

-- --------------------------------------------------------

--
-- Table structure for table `rt`
--

CREATE TABLE `rt` (
  `id_rt` int(11) NOT NULL,
  `nomor_rt` varchar(10) DEFAULT NULL,
  `nama_ketua_rt` varchar(100) DEFAULT NULL,
  `rw_id` int(10) UNSIGNED DEFAULT NULL,
  `dusun_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rt`
--

INSERT INTO `rt` (`id_rt`, `nomor_rt`, `nama_ketua_rt`, `rw_id`, `dusun_id`) VALUES
(1, '01', 'MUADZ', 3, 2),
(2, '02', 'NUR JANNA INA', 3, 2),
(3, '03', 'AHMAD HUDA', 3, 2),
(4, '01', 'WINARTI', 4, 2),
(5, '02', 'SUHADAK', 4, 2),
(6, '03', 'ABD. KHOLIK', 4, 2),
(7, '01', 'MARIYAM', 5, 2),
(8, '02', 'RIDWAN YUSUF', 5, 2),
(9, '03', 'M. KOLIDI', 5, 2),
(10, '01', 'IRKHAMMI', 6, 2),
(11, '02', 'MUSLIHIN AZIZ', 6, 2),
(12, '01', 'SUPARLAN', 7, 2),
(13, '02', 'ABDUL MUNTOLIB', 7, 2),
(14, '03', 'KASTAJI', 7, 2),
(15, '04', 'ULIL  ABSOR', 7, 2),
(16, '01', 'MUSLIH', 8, 2),
(17, '02', 'HADI S', 8, 2),
(18, '03', 'SUKIRNO', 8, 2),
(19, '04', 'ZAINUL ARIFIN', 8, 2),
(20, '05', 'ACHMAD SAICHU', 8, 2),
(21, '01', 'FADLAN', 9, 1),
(22, '02', 'Untung Sugiono', 9, 1),
(23, '03', 'Kurniansyah', 9, 1),
(24, '01', 'Saipudin', 10, 1),
(25, '02', 'Mujayyin', 10, 1),
(26, '03', 'Agus Mustalikan', 10, 1),
(27, '01', 'Roikah', 11, 1),
(28, '02', 'Aminoto', 11, 1),
(29, '03', 'Sabar', 11, 1),
(30, '04', 'Suparli', 11, 1),
(31, '05', 'Saifullah', 11, 1),
(32, '06', 'Saheri', 11, 1),
(33, '01', 'MADUKIN', 0, 1),
(34, '02', 'ROMLI', 12, 1),
(35, '03', 'ANITA', 12, 1),
(36, '04', 'SURYADI', 12, 1),
(37, '01', 'SAMSUL HUDA', 13, 1),
(38, '02', 'ZAINAL ARIFIN', 13, 1),
(39, '03', 'MUSRIKIN', 13, 1),
(40, '04', 'ABDUL KHOLIK', 13, 1),
(41, '01', 'NUR SHOLEH', 14, 1),
(42, '02', 'SLAMET RIYADI', 14, 1),
(43, '01', 'NUR QODIM', 15, 1),
(44, '02', 'EKO WAHYUDI', 15, 1),
(45, '01', 'ISNAFIN', 16, 3),
(46, '02', 'SUHARTO', 16, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rw`
--

CREATE TABLE `rw` (
  `id_rw` int(11) NOT NULL,
  `nomor_rw` varchar(10) DEFAULT NULL,
  `nama_ketua` varchar(50) DEFAULT NULL,
  `dusun_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rw`
--

INSERT INTO `rw` (`id_rw`, `nomor_rw`, `nama_ketua`, `dusun_id`) VALUES
(3, '01', 'AFANDI', 2),
(4, '02', 'H. M.LUKMAN', 2),
(5, '03', 'SYAJIDIN', 2),
(6, '04', 'SUPRIADI', 2),
(7, '05', 'KHOIRUL  ANAM', 2),
(8, '06', 'MASBUKIN', 2),
(9, '07', 'SUMISTI’AH', 1),
(10, '08', 'SULISTIYONO', 1),
(11, '09', 'HADI MULYO', 1),
(12, '10', 'KUSWANDI', 1),
(13, '11', 'SUKIRMAN', 1),
(14, '12', 'KHUSAINI', 1),
(15, '13', 'JIMI CAHYONO', 1),
(16, '14', 'WAJIB NURHADI', 3);

-- --------------------------------------------------------

--
-- Table structure for table `skck`
--

CREATE TABLE `skck` (
  `id_skck` int(11) NOT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `no_surat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sktm`
--

CREATE TABLE `sktm` (
  `id_sktm` int(11) NOT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `no_surat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `surat_kelahiran`
--

CREATE TABLE `surat_kelahiran` (
  `id_kelahiran` int(11) NOT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `nama_anak` varchar(100) DEFAULT NULL,
  `tmpt_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(50) DEFAULT NULL,
  `jk` enum('Laki-laki','Perempuan') DEFAULT NULL,
  `no_urut` int(11) DEFAULT NULL,
  `nama_ibu` varchar(100) DEFAULT NULL,
  `umur_ibu` int(11) DEFAULT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `umur_ayah` int(11) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `no_surat` varchar(100) DEFAULT NULL,
  `foto_kms` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat_kelahiran`
--

INSERT INTO `surat_kelahiran` (`id_kelahiran`, `tgl_dibuat`, `nama_anak`, `tmpt_lahir`, `tgl_lahir`, `agama`, `jk`, `no_urut`, `nama_ibu`, `umur_ibu`, `nama_ayah`, `umur_ayah`, `alamat`, `no_surat`, `foto_kms`) VALUES
(1, '2021-02-12', 'Anak Orang', 'Jombang', '2021-02-12', 'Islam', 'Laki-laki', 1, 'ENI SHOFA', 36, 'BUDI SUROSO', 42, 'Dsn. Catak Gayam Utara, RT. 002, RW. 004 Desa Catakgayam, Kecamatan   Mojowarno, Kabupaten Jombang', '100/1/415.71.14/2021', '62d8c56c446b31c8c09ccce74a74a1d3.png');

-- --------------------------------------------------------

--
-- Table structure for table `surat_kematian`
--

CREATE TABLE `surat_kematian` (
  `id_kematian` int(11) NOT NULL,
  `tgl_dibuat` date DEFAULT NULL,
  `nik_jenazah` varchar(16) DEFAULT NULL,
  `hari_meninggal` varchar(20) DEFAULT NULL,
  `tgl_meninggal` date DEFAULT NULL,
  `tmpt_meninggal` varchar(50) DEFAULT NULL,
  `sebab_meninggal` varchar(50) DEFAULT NULL,
  `nama_pelapor` varchar(50) DEFAULT NULL,
  `no_surat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `level` enum('admin','petugas') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `nama`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Novi', 'admin'),
(8, 'petugas', 'afb91ef692fd08c445e8cb1bab2ccf9c', 'Petugas', 'petugas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dusun`
--
ALTER TABLE `dusun`
  ADD PRIMARY KEY (`id_dusun`);

--
-- Indexes for table `penduduk`
--
ALTER TABLE `penduduk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengantar_nikah`
--
ALTER TABLE `pengantar_nikah`
  ADD PRIMARY KEY (`id_pengantar_nikah`);

--
-- Indexes for table `profil_desa`
--
ALTER TABLE `profil_desa`
  ADD PRIMARY KEY (`id_profil_desa`);

--
-- Indexes for table `rt`
--
ALTER TABLE `rt`
  ADD PRIMARY KEY (`id_rt`);

--
-- Indexes for table `rw`
--
ALTER TABLE `rw`
  ADD PRIMARY KEY (`id_rw`);

--
-- Indexes for table `skck`
--
ALTER TABLE `skck`
  ADD PRIMARY KEY (`id_skck`);

--
-- Indexes for table `sktm`
--
ALTER TABLE `sktm`
  ADD PRIMARY KEY (`id_sktm`);

--
-- Indexes for table `surat_kelahiran`
--
ALTER TABLE `surat_kelahiran`
  ADD PRIMARY KEY (`id_kelahiran`);

--
-- Indexes for table `surat_kematian`
--
ALTER TABLE `surat_kematian`
  ADD PRIMARY KEY (`id_kematian`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dusun`
--
ALTER TABLE `dusun`
  MODIFY `id_dusun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penduduk`
--
ALTER TABLE `penduduk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `pengantar_nikah`
--
ALTER TABLE `pengantar_nikah`
  MODIFY `id_pengantar_nikah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rt`
--
ALTER TABLE `rt`
  MODIFY `id_rt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `rw`
--
ALTER TABLE `rw`
  MODIFY `id_rw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `skck`
--
ALTER TABLE `skck`
  MODIFY `id_skck` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sktm`
--
ALTER TABLE `sktm`
  MODIFY `id_sktm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `surat_kelahiran`
--
ALTER TABLE `surat_kelahiran`
  MODIFY `id_kelahiran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `surat_kematian`
--
ALTER TABLE `surat_kematian`
  MODIFY `id_kematian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
